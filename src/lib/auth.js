import auth0 from 'auth0-js';

const createAuth0Instance = () => new auth0.WebAuth({
  domain: process.env.REACT_APP_AUTH0_DOMAIN,
  clientID: process.env.REACT_APP_AUTH0_CLIENT_ID,
  redirectUri: process.env.REACT_APP_REDIRECT_URI,
  audience: process.env.REACT_APP_AUTH0_AUDIENCE,
  responseType: 'token id_token',
  scope: 'openid profile email',
});

const login = (cb) => {
  createAuth0Instance().authorize(cb);
};

const setSession = (authResult) => {
  const expiresAt = JSON.stringify(
    authResult.expiresIn * 1000 + new Date().getTime(),
  );

  localStorage.setItem('access_token', authResult.accessToken);
  localStorage.setItem('id_token', authResult.idToken);
  localStorage.setItem('expires_at', expiresAt);
};

const handleAuthentication = (cb) => {
  const auth = createAuth0Instance();
  auth.parseHash((err, authResult) => {
    if (err) {
      auth.logout({
        returnTo: 'https://launch.mergetb.net',
        client_id: process.env.REACT_APP_AUTH0_CLIENT_ID,
      });
      cb(err);
    } else if (authResult && authResult.accessToken && authResult.idToken) {
      setSession(authResult);
      cb(null, authResult);
    } else {
      auth.logout({
        returnTo: 'https://launch.mergetb.net',
        client_id: process.env.REACT_APP_AUTH0_CLIENT_ID,
      });
      cb(new Error('Unexpected format of authResult'));
    }
  });
};

const logout = () => {
  localStorage.removeItem('access_token');
  localStorage.removeItem('id_token');
  localStorage.removeItem('expires_at');
  createAuth0Instance().logout({
    returnTo: 'https://launch.mergetb.net',
    client_id: process.env.REACT_APP_AUTH0_CLIENT_ID,
  });
};

const isAuthenticated = () => {
  if (
    !localStorage.getItem('access_token')
        || !localStorage.getItem('id_token')
        || !localStorage.getItem('expires_at')
  ) {
    return false;
  }

  const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
  const valid = new Date().getTime() < expiresAt;
  return valid;
};

const isExpired = () => {
  if (!localStorage.getItem('expires_at')) {
    return false; // no token, so cannot be expired.
  }
  const expAt = JSON.parse(localStorage.getItem('expires_at'));
  const expired = new Date().getTime() >= expAt;
  return expired;
};

const getAccessToken = () => {
  const accessToken = localStorage.getItem('access_token');
  return accessToken;
};

const getIDToken = () => {
  const token = localStorage.getItem('id_token');

  if (!token) {
    throw new Error('No ID token found');
  }
  return token;
};

const getProfile = (cb) => {
  const accessToken = getAccessToken();

  createAuth0Instance().client.userInfo(accessToken, (err, profile) => {
    if (err) {
      cb(err);
      return;
    }

    cb(null, profile);
  });
};

export default {
  getProfile,
  getAccessToken,
  getIDToken,
  handleAuthentication,
  isAuthenticated,
  login,
  logout,
  setSession,
  isExpired,
};
