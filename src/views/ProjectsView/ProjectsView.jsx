import React from 'react';
import ProjectsList from 'components/ProjectsList/ProjectsList';

const ProjectsView = () => <ProjectsList />;

export default ProjectsView;
