import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';

import UsersList from 'components/UsersList/UsersList';
import PubKeysList from 'components/PubKeysList/PubKeysList';
import { getAuthdProfile } from 'reducers/selectors';

const UsersView = (props) => {
  const { profile } = props;
  return (
    <div className="content">
      <Container fluid>
        <Row>
          <Col md={6}>
            <UsersList />
          </Col>
          <Col md={6}>
            <PubKeysList profile={profile} />
          </Col>
        </Row>
      </Container>
    </div>
  );
};

const mapStateToProps = (state) => ({
  profile: getAuthdProfile(state),
});

export default connect(mapStateToProps)(UsersView);
