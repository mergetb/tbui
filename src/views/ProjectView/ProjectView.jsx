/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
  Card, Container, Row, Col,
} from 'react-bootstrap';

import ProjectProfile from 'components/ProjectProfile/ProjectProfile';
import ExperimentsList from 'components/Experiment/ExperimentsList';
import MembersList from 'components/MembersList/MembersList';

const ProjectView = (props) => {
  const { match } = props;
  const { params } = match;
  const { projectname } = params;
  return (
    <Card>
      <Card.Header>
        Project:
        {' '}
        {projectname}
      </Card.Header>
      <Card.Body>
        <Container fluid>
          <Row>
            <Col md={6}>
              <ProjectProfile {...props} />
            </Col>
            <Col md={6}>
              <ExperimentsList {...props} />
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <MembersList {...props} />
            </Col>
          </Row>
        </Container>
      </Card.Body>
    </Card>
  );
};

export default ProjectView;
