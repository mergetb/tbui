/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
  Card, Container, Row, Col,
} from 'react-bootstrap';

import ExperimentProfile from 'components/ExperimentProfile/ExperimentProfile';
import ExpVersionList from 'components/Experiment/ExpVersionList';
import ExpXdcList from 'components/Experiment/ExpXdcList';
import RealizationList from 'components/Realization/RealizationList';

const ExperimentView = (props) => {
  // eslint-disable-next-line react/destructuring-assignment
  const { projectname, experimentname } = props.match.params;
  return (
    <div className="content">
      <Container fluid>
        <Row>
          <Col md={12}>
            <Card>
              <Card.Header>
                Experiment:
                {' '}
                <strong>{`${projectname} / ${experimentname}`}</strong>
              </Card.Header>
              <Card.Body>
                <Container fluid>
                  <Row>
                    <Col md={8}>
                      <Col>
                        <ExpVersionList {...props} />
                      </Col>
                      <Col>
                        <RealizationList {...props} />
                      </Col>
                      <Col>
                        <ExpXdcList {...props} />
                      </Col>
                    </Col>
                    <Col md={4}>
                      <ExperimentProfile {...props} />
                    </Col>
                  </Row>
                </Container>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default ExperimentView;
