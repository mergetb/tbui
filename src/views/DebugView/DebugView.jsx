import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import ShowReduxState from 'components/Debug/ShowReduxState';

const DebugView = () => (
  <Container fluid>
    <Row>
      <Col>
        <ShowReduxState />
      </Col>
    </Row>
  </Container>
);

export default DebugView;
