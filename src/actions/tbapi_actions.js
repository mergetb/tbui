import Auth from 'lib/auth';
import * as selectors from 'reducers/selectors';
import * as nas from 'actions/notification_actions';
import { tokenExpired } from 'actions/auth_actions';

// types of actions we support.
export const types = {
  FETCHING_BY_KEY: 'FETCHING_BY_KEY',
  RECEIVE: '_RECEIVE',
  UPDATE_NAMED_INDEXED_ENTRY: '_UPDATE_NAMED_INDEX_ENTRY',
  DELETE_NAMED_INDEXED_ENTRY: '_DELETE_NAMED_INDEX_ENTRY',

  RECEIVE_ARRAY_OF_KEYED_OBJECTS: 'RECEIVE_ARRAY_OF_KEYED_OBJECTS',
  RECEIVE_OBJECT_BY_ID: 'RECEIVE_OBJECT_BY_ID',
  DELETE_OBJECT_BY_ID: 'DELETE_OBJECT_BY_ID',
};

export const prefixes = {
  USERS: 'users',
  PROJECTS: 'projects',
};

const objectkeys = {
  USER: 'username',
  PROJECT: 'name',
};

const setNetworkError = { type: 'NETWORK_ERROR' };
const userNotActive = { type: 'USER_NOT_ACTIVE' };

// We use scoped actions and reducers to reuse actions/reducer code

// associate a value (boolean), with a key while it's being downloaded/fetched so
// we don't fetch it over and over while waiting.
const setFetchingByKey = (key, value) => ({
  type: types.FETCHING_BY_KEY,
  key,
  value,
});

// store an array of objects. the objects have keys in themselves that
// uniquely ID the object.
const receiveArrayOfKeyedObjectsFor = (prefix, key) => {
  const receiveArrayOfKeyedObjects = (payload) => ({
    type: prefix + types.RECEIVE_ARRAY_OF_KEYED_OBJECTS,
    key,
    payload,
  });
  return receiveArrayOfKeyedObjects;
};

const receiveObjectByIdFor = (prefix) => {
  const receiveObjectById = (id, payload) => ({
    type: prefix + types.RECEIVE_OBJECT_BY_ID,
    id,
    payload,
  });
  return receiveObjectById;
};

const deleteObjectByIdFor = (prefix) => {
  const deleteObjectById = (id) => ({
    type: prefix + types.DELETE_OBJECT_BY_ID,
    id,
  });
  return deleteObjectById;
};

const updateNamedEntryFor = (prefix) => {
  const updateNamedEntry = (name, index, json) => ({
    type: prefix + types.UPDATE_NAMED_INDEXED_ENTRY,
    name,
    index,
    json,
  });
  return updateNamedEntry;
};

const deleteNamedEntryFor = (prefix) => {
  const deleteNamedEntry = (name, index) => ({
    type: prefix + types.DELETE_NAMED_INDEXED_ENTRY,
    name,
    index,
  });
  return deleteNamedEntry;
};

// single top level entry in state. Can be anything.
const setPayloadInStateFor = (prefix) => {
  const setPayloadInState = (payload) => ({
    type: prefix + types.RECEIVE,
    payload,
  });
  return setPayloadInState;
};

const UsersReceive = receiveArrayOfKeyedObjectsFor(prefixes.USERS, objectkeys.USER);
const UserReceive = receiveObjectByIdFor(prefixes.USERS);
const UserDelete = deleteObjectByIdFor(prefixes.USERS);

const ProjectsReceive = receiveArrayOfKeyedObjectsFor(prefixes.PROJECTS, objectkeys.PROJECT);
const ProjectReceive = receiveObjectByIdFor(prefixes.PROJECTS);
const ProjectDelete = deleteObjectByIdFor(prefixes.PROJECTS);

// For experiments and members  for a given project, we first request the list
// of names, then request each one by name.
const ExperimentReceive = updateNamedEntryFor('experiments');
const ExperimentsReceive = updateNamedEntryFor('experiments');
const ExperimentDelete = deleteNamedEntryFor('experiments');
const ExperimentVersionReceive = updateNamedEntryFor('expversion');
const ExperimentVersionsReceive = updateNamedEntryFor('expversions'); // note the 's'
const RealizationReceive = updateNamedEntryFor('realization');
const RealizationsReceive = updateNamedEntryFor('realizations'); // Note the 's'
const RealizationDelete = deleteNamedEntryFor('realization');
const MaterializationReceive = updateNamedEntryFor('materialization');
const MaterializationDelete = deleteNamedEntryFor('materialization');
const MaterializationStatusReceive = updateNamedEntryFor('materializationStatus');
const MaterializationStatusDelete = deleteNamedEntryFor('materializationStatus');

const MemberReceive = updateNamedEntryFor('members');
const MemberDelete = deleteNamedEntryFor('members');

const PublicKeysReceieve = updateNamedEntryFor('pubkeys');
const PublicKeyDelete = deleteNamedEntryFor('pubkeys');

const ExpXdcsReceive = updateNamedEntryFor('expxdcs'); // Note the 's'
const ExpXdcReceive = updateNamedEntryFor('expxdcs'); // Note the 's'
const ExpXdcDelete = deleteNamedEntryFor('expxdcs');
const XdcTokenReceive = updateNamedEntryFor('xdctoken');

const MaterializationTransition = setPayloadInStateFor('mtzTransition');

// Health is just a bool.
const HealthReceive = setPayloadInStateFor('health');

//
// Utils.
//
// endpoint manipulations are not "real" actions in the redux sense.
const getAPIEndpoint = () => {
  const address = localStorage.getItem('merge_apiEndpoint');
  if (!address) {
    if (process.env.REACT_APP_API_ENDPOINT) {
      return process.env.REACT_APP_API_ENDPOINT;
    }
    return '';
  }
  return address;
};
const setAPIEndpoint = (endpoint) => {
  localStorage.setItem('merge_apiEndpoint', endpoint);
};

const getUrl = (path, dispatch) => {
  const apiEndpoint = getAPIEndpoint();
  if (!apiEndpoint) {
    return { url: null, opts: null, error: 'Testbed API Endpoint not set.' };
  }

  if (Auth.isExpired()) {
    dispatch(tokenExpired());
    return { url: null, opts: null, error: 'Authenication token expired.' };
  }

  const url = `https://${apiEndpoint}/${path}`;
  const token = Auth.getAccessToken();
  if (!token) {
    return { url: null, opts: null };
  }

  const options = {
    method: 'get',
    headers: new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    }),
  };
  return { url, opts: options };
};

const handleUrlError = (urlinfo, dispatch) => {
  if (Object.hasOwnProperty.call(urlinfo, 'error')) {
    return dispatch(nas.SetNotifcation(nas.Level.ERROR, urlinfo.error));
  }
  return null;
};

//
// Async action creators with redux-thunk
//

//
//
//
function fetchThingIfNeeded(getThing, isFetching, fetchThing) {
  return (dispatch, getState) => {
    const state = getState();

    if (state.networkError === true) {
      return Promise.reject();
    }

    const thing = getThing(state);
    const fetching = isFetching(state);
    let fetchIt = false;

    if (!thing && !fetching) fetchIt = true; // we don't have it and we're not getting it.

    if (fetchIt) {
      return dispatch(fetchThing());
    }
    // else
    return Promise.resolve(); // do nothing, but allow an then().then()... chain to continue.
  };
}

const fetchThing = (path, receiveAction, handleError) => (dispatch) => {
  const urlinfo = getUrl(path, dispatch);
  if (!urlinfo.url) {
    return handleUrlError(urlinfo, dispatch);
  }
  dispatch(setFetchingByKey(path, true));

  return fetch(urlinfo.url, urlinfo.opts)
    .then(
      (response) => {
        if (!response.ok) {
          if (handleError) {
            return handleError(dispatch, response);
          }
        }
        const contentType = response.headers.get('content-type');
        if (contentType && contentType.indexOf('application/json') !== -1) {
          return response.json();
        }
        return null;
      },
      (error) => {
        dispatch(setNetworkError);
        dispatch(nas.SetNotifcation(nas.Level.CRITICAL, `fetch Error getting ${urlinfo.url}: ${error}`));
        dispatch(nas.SetNotifcation(nas.Level.INFO, 'Can you connect to the Merge API?'));
        return null;
      },
    )
    .then((json) => {
      if (json) {
        dispatch(receiveAction(json, dispatch));
      }
      dispatch(setFetchingByKey(path, false));
    })
    .catch((error) => {
      dispatch(setNetworkError);
      dispatch(nas.SetNotifcation(nas.Level.CRITICAL, `Network Error: ${urlinfo.url}: ${error}`));
      dispatch(nas.SetNotifcation(nas.Level.CRITICAL, 'Please fix and then refresh the application.'));
    });
};

const fetchHealth = () => (dispatch) => {
  const path = 'health';
  const urlinfo = getUrl(path, dispatch);
  if (!urlinfo.url) {
    return handleUrlError(urlinfo, dispatch);
  }
  dispatch(setFetchingByKey(path, true));
  return fetch(urlinfo.url, urlinfo.opts)
    .then(
      (response) => {
        if (response.ok) {
          if (response.statusText === 'OK') {
            dispatch(HealthReceive(true));
            dispatch(setFetchingByKey(path, false));
          }
        } else {
          dispatch(nas.SetNotifcation(nas.Level.ERROR, `Error: server response: ${response.status}`));
          dispatch(HealthReceive(false));
          dispatch(setFetchingByKey(path, false));
        }
      },
      (error) => { // eslint-disable-line no-unused-vars
        dispatch(nas.SetNotifcation(nas.Level.ERROR, 'Unable to contact API endpoint.'));
        dispatch(HealthReceive(false));
        dispatch(setFetchingByKey(path, false));
      },
    );
};

const fetchHealthIfNeeded = () => fetchThingIfNeeded(
  selectors.getHealth,
  selectors.isFetchingHealth,
  fetchHealth,
);

function fetchUsers() {
  return fetchThing(
    'users',
    UsersReceive,
  );
}

const fetchUserError = (dispatch, response) => {
  // If the response.status is 403, the user has not been activated on the testbed yet
  // (as per the mergetb open api docs). So we need to set state to let components
  // know the user should not attempt to access anything.
  if (response.status_code === 403) {
    return dispatch(userNotActive);
  }
  // Something smart should happen here.
  return null;
};

function fetchUser(name) {
  return fetchThing(
    `users/${name}`,
    (payload) => UserReceive(name, payload),
    fetchUserError,
  );
}

const fetchUserIfNeeded = (username) => fetchThingIfNeeded(
  (state) => selectors.getUser(state, username),
  (state) => selectors.isFetchingUser(state, username),
  () => fetchUser(username),
);

const fetchUsersIfNeeded = () => fetchThingIfNeeded(
  selectors.getUsers,
  selectors.isFetchingUsers,
  fetchUsers,
);

function fetchProjects() {
  return fetchThing(
    'projects',
    ProjectsReceive,
  );
}

const fetchProjectsIfNeeded = () => fetchThingIfNeeded(
  selectors.getProjects,
  selectors.isFetchingProjects,
  fetchProjects,
);

function fetchProject(pid) { // eslint-disable-line no-unused-vars
  return fetchThing(
    `projects/${pid}`,
    (payload) => ProjectReceive(pid, payload),
  );
}

const fetchProjectIfNeeded = (pid) => fetchThingIfNeeded(
  (state) => selectors.getProject(state, pid),
  (state) => selectors.isFetchingProject(state, pid),
  () => fetchProject(pid),
);

const fetchExperiment = (projectname, expname) => fetchThing(
  `projects/${projectname}/experiments/${expname}`,
  (exp) => ExperimentReceive(projectname, expname, exp),
);

const fetchExperimentIfNeeded = (projectname, expname) => fetchThingIfNeeded(
  (state) => selectors.getExperiment(state, projectname, expname),
  (state) => selectors.isFetchingExperiment(state, projectname, expname),
  () => fetchExperiment(projectname, expname),
);

const fetchExperiments = (projectname) => (dispatch) => {
  const path = `projects/${projectname}/experiments`;
  const urlinfo = getUrl(path, dispatch);
  if (!urlinfo.url) {
    return handleUrlError(urlinfo, dispatch);
  }
  let expnames = [];
  dispatch(setFetchingByKey(path, true));
  return fetch(urlinfo.url, urlinfo.opts)
    .then(
      (response) => {
        if (!response.ok) {
          dispatch(nas.SetNotifcation(
            nas.Level.CRITICAL, `(experiment) Error getting ${path}: ${response.statusText}`,
          ));
          return Promise.reject();
        }
        return response.json();
      },
      (error) => {
        dispatch(setFetchingByKey(path, false));
        dispatch(setNetworkError);
        dispatch(nas.SetNotifcation(nas.Level.ERROR, `EXP Net: Error getting ${urlinfo.url}: ${error}`));
        dispatch(nas.SetNotifcation(nas.Level.CRITICAL, 'Please fix and then refresh the application.'));
      },
    )
    .then((json) => {
      expnames = json;
    })
    .then(() => {
      if (expnames.length === 0) {
        dispatch(ExperimentsReceive(projectname, null, {})); // set to empty.
        dispatch(setFetchingByKey(path, false));
      } else {
        Promise.all(
          expnames.map((exp) => dispatch(fetchExperiment(projectname, exp))),
        ).then(() => { dispatch(setFetchingByKey(path, false)); });
      }
    });
};

const fetchExperimentsIfNeeded = (projectname) => fetchThingIfNeeded(
  (state) => selectors.getExperiments(state, projectname),
  (state) => selectors.isFetchingExperiments(state, projectname),
  () => fetchExperiments(projectname),
);

function fetchMember(projectname, membername) {
  return fetchThing(
    `projects/${projectname}/members/${membername}`,
    (member) => MemberReceive(projectname, membername, member),
  );
}

const fetchMembers = (projectname) => (dispatch) => {
  const path = `projects/${projectname}/members`;
  const urlinfo = getUrl(path, dispatch);
  if (!urlinfo.url) {
    return handleUrlError(urlinfo, dispatch);
  }
  dispatch(setFetchingByKey(path, true));
  return fetch(urlinfo.url, urlinfo.opts)
    .then(
      (response) => {
        if (!response.ok) {
          dispatch(nas.SetNotifcation(
            nas.Level.CRITICAL, `Error getting ${path}: ${response.statusText}`,
          ));
          return Promise.reject();
        }
        return response.json();
      },
      (error) => {
        dispatch(setFetchingByKey(path, false));
        dispatch(nas.SetNotifcation(nas.Level.CRITICAL, `Error getting ${urlinfo.url}: ${error}`));
        dispatch(nas.SetNotifcation(nas.Level.CRITICAL, 'Please fix and then refresh the application.'));
        dispatch(setNetworkError);
      },
    )
    .then((json) => {
      // Just get all the members now. Wrap all the fetches in a promise
      // and wait for them all to finish before setting the members to
      // fully feched.
      Promise.all(
        json.map((name) => dispatch(fetchMember(projectname, name))),
      ).then(() => {
        dispatch(setFetchingByKey(path, false));
      });
    });
};

const fetchMembersIfNeeded = (projectname) => fetchThingIfNeeded(
  (state) => selectors.getMembers(state, projectname),
  (state) => selectors.isFetchingMembers(state, projectname),
  () => fetchMembers(projectname),
);

const fetchRealizations = (projname, expname) => fetchThing(
  `projects/${projname}/experiments/${expname}/realizations`,
  (reals) => RealizationsReceive(projname, expname, reals),
);

const fetchRealizationsIfNeeded = (projname, expname) => fetchThingIfNeeded(
  (state) => selectors.getRealizations(state, projname, expname),
  (state) => selectors.isFetchingRealizations(state, projname, expname),
  () => fetchRealizations(projname, expname),
);

const fetchRealization = (pid, eid, rid) => fetchThing(
  `projects/${pid}/experiments/${eid}/realizations/${rid}`,
  (rlz, dispatch) => dispatch(RealizationReceive([pid, eid].join(), rid, rlz)), // horrible!
);

const fetchRealizationIfNeeded = (pid, eid, rid) => fetchThingIfNeeded(
  (state) => selectors.getRealization(state, pid, eid, rid),
  (state) => selectors.isFetchingRealization(state, pid, eid, rid),
  () => fetchRealization(pid, eid, rid),
);

//
// Checks
//

const initUser = () => fetchThing('user/init', () => {});

//
// Generic CRUD
//
const doFetchAction = (path, body, fetchAction, handleResponse) => (dispatch) => {
  const urlinfo = getUrl(path, dispatch);
  if (!urlinfo.url) {
    return handleUrlError(urlinfo, dispatch);
  }
  const validActions = ['PUT', 'POST', 'DELETE'];
  if (!validActions.includes(fetchAction)) {
    return dispatch(nas.SetNotifcation(nas.Level.ERROR, `bad fetch action: ${fetchAction}`));
  }
  return fetch(urlinfo.url, { ...urlinfo.opts, method: fetchAction, body: JSON.stringify(body) })
    .then(
      (response) => dispatch(handleResponse(response, path, body, fetchAction)),
      (error) => dispatch(nas.SetNotifcation(nas.Level.CRITICAL,
        `Error in ${fetchAction} ${urlinfo.url}: ${error}`)),
    );
};

const doFetchPost = (path, body, handleResponse) => doFetchAction(path, body, 'POST', handleResponse);
const doFetchPut = (path, body, handleResponse) => doFetchAction(path, body, 'PUT', handleResponse);
const doFetchDelete = (path, body, handleResponse) => doFetchAction(path, body, 'DELETE', handleResponse);

//
// CRUD Projects
//
const putPostProjectResponse = (response, path, postedBody) => (dispatch) => {
  const pid = postedBody.name;
  if (response.ok) {
    return dispatch(fetchProject(pid));
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error creating/updating project ${pid} : ${response.statusText}`));
};

const postProject = (pid, desc, mode) => doFetchPost(
  `/projects/${pid}`,
  {
    name: pid,
    description: desc,
    accessMode: mode,
  },
  putPostProjectResponse,
);

const putProject = (pid, desc, mode) => doFetchPut(
  `/projects/${pid}`,
  {
    name: pid,
    description: desc,
    accessMode: mode,
  },
  putPostProjectResponse,
);

const putProjectMemberResponse = (response, path, body) => (dispatch) => {
  if (response.ok) {
    dispatch(fetchMembers(body.project));
    return dispatch(nas.SetNotifcation(nas.Level.SUCCESS,
      `Added ${body.member} to project ${body.project}`));
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error adding ${body.member} to project ${body.project}`));
};

const putProjectMember = (pid, uid) => doFetchPut(
  `/projects/${pid}/members/${uid}`,
  {
    member: uid,
    project: pid,
    role: 'member',
    state: 'active',
  },
  putProjectMemberResponse,
);

const deleteProjectMemberResponse = (response, path, body) => (dispatch) => {
  const [,, pid,, mid] = path.split('/');
  if (response.ok) {
    dispatch(MemberDelete(pid, mid));
    dispatch(fetchMembers(pid));
    return dispatch(nas.SetNotifcation(nas.Level.SUCCESS,
      `Removed ${body.member} from project ${body.project}`));
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error removing member from project ${pid}`));
};

const deleteProjectMember = (pid, mid) => doFetchDelete(
  `/projects/${pid}/members/${mid}`,
  {},
  deleteProjectMemberResponse,
);

const deleteProjectResponse = (resp, path) => (dispatch) => {
  const [,, pid] = path.split('/');
  if (resp.ok) {
    return dispatch(ProjectDelete(pid));
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error deleting project ${pid} : ${resp.statusText}`));
};

const deleteProject = (projname) => doFetchDelete(
  `/projects/${projname}`,
  {},
  deleteProjectResponse,
);
//
// CRUD Experiments
//
const putPostExperimentResponse = (response, path) => (dispatch) => {
  const [,, pid, , eid] = path.split('/');
  if (response.ok) {
    return dispatch(fetchExperiment(pid, eid));
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error creating/updating experiment ${pid}/${eid} : ${response.statusText}`));
};

const postExperiment = (pid, eid, desc) => doFetchPost(
  `/projects/${pid}/experiments/${eid}`,
  {
    name: eid,
    description: desc,
    project: pid,

  },
  putPostExperimentResponse,
);

const putExperiment = (pid, eid, desc) => doFetchPut(
  `/projects/${pid}/experiments/${eid}`,
  {
    name: eid,
    description: desc,
  },
  putPostExperimentResponse,
);

const deleteExperimentResponse = (resp, path) => (dispatch) => {
  // there is probably a much better way to pass these in.
  const [,, pid, , eid] = path.split('/');
  if (resp.ok) {
    // GTL unset fetching for this experiment as well.
    // so we'll get it again if it is recreated.
    return dispatch(ExperimentDelete(pid, eid));
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error deleting experiment ${pid}/${eid} : ${resp.statusText}`));
};

const deleteExperiment = (pid, eid) => doFetchDelete(
  `/projects/${pid}/experiments/${eid}`,
  {},
  deleteExperimentResponse,
);
//
// CRUD Realizations
//
const putRealizationResponse = (resp, path) => (dispatch) => {
  const [,, pid, , eid, , rid] = path.split('/');
  if (resp.ok) {
    dispatch(RealizationDelete([pid, eid].join(), rid));
    dispatch(fetchRealizations(pid, eid));
  }

  return resp;
};

const putRealization = (pid, eid, hash, rid) => doFetchPut(
  `/projects/${pid}/experiments/${eid}/realizations/${rid}`,
  {
    name: rid,
    hash,
  },
  putRealizationResponse,
);

const deleteRealizationResponse = (resp, path) => (dispatch) => {
  // there is probably a much better way to pass these in.
  const [,, pid, , eid, , rid] = path.split('/');
  if (resp.ok) {
    dispatch(fetchRealizations(pid, eid)); // get updated information after delete.
    return dispatch(RealizationDelete([pid, eid].join(), rid)); // remove from local store.
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error deleting realization ${pid}/${eid}/${rid} : ${resp.statusText}`));
};

const deleteRealization = (pid, eid, rid) => doFetchDelete(
  `/projects/${pid}/experiments/${eid}/realizations/${rid}`,
  {},
  deleteRealizationResponse,
);
const realizationAcceptActionResponse = (resp, path, body) => (dispatch) => {
  const [,, pid, , eid, , rid] = path.split('/');
  if (resp.ok) {
    if (body.action === 'accept') {
      return dispatch(nas.Info(`Accepted realization: ${pid}/${eid}/${rid}`));
    }
    dispatch(fetchRealizations(pid, eid)); // get updated information after delete.
    return dispatch(nas.Info(`Rejected realization: ${pid}/${eid}/${rid}`));
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error doing action ${body} for ${pid}/${eid}/${rid} : ${resp.statusText}`));
};

const realizationAcceptAction = (accept, pid, eid, rid) => doFetchPost(
  `/projects/${pid}/experiments/${eid}/realizations/${rid}/act`,
  { action: accept ? 'accept' : 'reject' },
  realizationAcceptActionResponse,
);

const fetchMaterialization = (pid, eid, rid) => fetchThing(
  `/projects/${pid}/experiments/${eid}/realizations/${rid}/materialization`,
  (mtz) => MaterializationReceive([pid, eid].join(), rid, mtz.mzMap),
  (dispatch, response) => {
    if (response.status === 404) {
      dispatch(MaterializationDelete([pid, eid].join(), rid));
    }
  },
);

const fetchMaterializationIfNeeded = (pid, eid, rid) => fetchThingIfNeeded(
  (state) => selectors.getMaterialization(state, pid, eid, rid),
  (state) => selectors.isFetchingMaterialization(state, pid, eid, rid),
  () => fetchMaterialization(pid, eid, rid),
);

const fetchMaterializationStatus = (pid, eid, rid) => fetchThing(
  `/projects/${pid}/experiments/${eid}/realizations/${rid}/materialization/status`,
  (mats) => MaterializationStatusReceive([pid, eid].join(), rid, mats),
  (dispatch, response) => {
    if (response.status === 404) {
      // Clear any status on 404.
      dispatch(MaterializationStatusDelete([pid, eid].join(), rid));
    }
  },
);

const putMaterializationResponse = (resp, path) => (dispatch) => {
  const [,, pid, , eid, , rid] = path.split('/');
  if (resp.ok) {
    return dispatch(fetchMaterialization(pid, eid, rid));
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error creating materialization for ${pid}/${eid}/${rid} : ${resp.statusText}`));
};

const putMaterialization = (pid, eid, rid) => doFetchPut(
  `/projects/${pid}/experiments/${eid}/realizations/${rid}/materialization`,
  { },
  putMaterializationResponse,
);

// const fetchMaterializationStatusIfNeeded = (pid, eid, rid) => {
//     return fetchThingIfNeeded(
//         (state) => selectors.getMaterializationStatus(state, pid, eid, rid),
//         (state) => selectors.isFetchingMaterializations(state, pid, eid, rid),
//         () => fetchMaterializations(pid, eid, rid)
//     )
// }

const deleteMaterializationResponse = (resp, path) => (dispatch) => {
  // there is probably a much better way to pass these in.
  const [,, pid, , eid, , rid] = path.split('/');
  if (resp.ok) {
    const key = [pid, eid].join();
    dispatch(MaterializationDelete(key, rid));
    return dispatch(fetchMaterialization(pid, eid, rid)); // reload.
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error deleting materialization ${pid}/${eid}/${rid} : ${resp.statusText}`));
};

const deleteMaterialization = (pid, eid, rid) => doFetchDelete(
  `/projects/${pid}/experiments/${eid}/realizations/${rid}/materialization`,
  {},
  deleteMaterializationResponse,
);

// fetch the array of hashes which are the "versions". Then get data about each
// version/experiment src.
const fetchExperimentVersions = (pid, eid) => {
  const path = `projects/${pid}/experiments/${eid}/src`;
  return fetchThing(
    path,
    (hashes) => ExperimentVersionsReceive(pid, eid, hashes),
  );
};

const postExpVersionResponse = (resp, path) => (dispatch) => {
  const [,, pid, , eid] = path.split('/');
  if (resp.ok) {
    // reload all versions, because I don't have time to do it right.
    // Doing it right would be getting the new list of hashes and GETting just the new hash
    // that was just created.
    return dispatch(fetchExperimentVersions(pid, eid));
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error creating experiment version for ${pid}/${eid} : ${resp.statusText}`));
};

const postExpVersion = (pid, eid, xir, src) => doFetchPost(
  `/projects/${pid}/experiments/${eid}/src`,
  { xir, src },
  postExpVersionResponse,
);

const fetchExperimentVersionsIfNeeded = (pid, eid) => fetchThingIfNeeded(
  (state) => selectors.getExperimentVersions(state, pid, eid),
  (state) => selectors.isFetchingExperimentVersions(state, pid, eid),
  () => fetchExperimentVersions(pid, eid),
);

const fetchExperimentVersion = (pid, eid, hash) => fetchThing(
  `projects/${pid}/experiments/${eid}/src/${hash}`,
  (expSrc) => {
    // parse the src into JSON for ease of use in GUI
    expSrc.xir = JSON.parse(expSrc.xir); // eslint-disable-line no-param-reassign
    return ExperimentVersionReceive([pid, eid].join(), hash, expSrc);
  },
);

const fetchExperimentVersionIfNeeded = (pid, eid, hash) => fetchThingIfNeeded(
  (state) => selectors.getExperimentVersion(state, pid, eid, hash),
  (state) => selectors.isFetchingExperimentVersion(state, pid, eid, hash),
  () => fetchExperimentVersion(pid, eid, hash),
);

const putPutUserResponse = (response, path, postedBody) => (dispatch) => {
  const uid = postedBody.username;
  if (response.ok) {
    return dispatch(fetchUser(uid));
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error creating user ${uid} : ${response.statusText}`));
};

const putUser = (uid, fullname, email) => doFetchPut(
  `/users/${uid}`,
  {
    username: uid,
    name: fullname,
    email,
  },
  putPutUserResponse,
);

const deleteUserResponse = (resp, path, body) => (dispatch) => {
  const uid = body.username;
  if (resp.ok) {
    return dispatch(UserDelete(uid));
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error deleting user ${uid} : ${resp.statusText}`));
};

const deleteUser = (uid) => doFetchDelete(
  `/users/${uid}`,
  {},
  deleteUserResponse,
);

const loadingExpXdc = (pid, eid, xdcid) => ExpXdcReceive(
  pid,
  eid,
  {
    name: xdcid,
    fdqn: '',
    link: '',
  },
);

const getExpXdcs = (projname, expname) => fetchThing(
  `projects/${projname}/experiments/${expname}/xdc`,
  (xdcs) => ExpXdcsReceive(projname, expname, xdcs),
);

const putExpXdcResponse = (resp, path) => (dispatch) => {
  const [,, pid, , eid, , xdcid] = path.split('/');
  if (!resp.ok) {
    dispatch(nas.SetNotifcation(nas.Level.ERROR,
      `Error creating experiment XDC ${pid}/${eid}/${xdcid} : ${resp.statusText}`));
  }
  return dispatch(getExpXdcs(pid, eid)); // update with new data.
};


const putExpXdc = (pid, eid, xdcid) => doFetchPut(
  `/projects/${pid}/experiments/${eid}/xdc/${xdcid}`, {},
  putExpXdcResponse,
);

const fetchExpXdcsIfNeeded = (projname, expname) => fetchThingIfNeeded(
  (state) => selectors.getExpXdcs(state, projname, expname),
  (state) => selectors.isFetchingExpXdcs(state, projname, expname),
  () => getExpXdcs(projname, expname),
);

const getXdcToken = (pid, eid, xdcname) => fetchThing(
  `projects/${pid}/experiments/${eid}/xdc/${xdcname}/token`,
  (token) => XdcTokenReceive([pid, eid].join(), xdcname, token),
);

const fetchXdcTokenIfNeeded = (pid, eid, xdcid) => fetchThingIfNeeded(
  (state) => selectors.getXdcToken(state, pid, eid, xdcid),
  (state) => selectors.isFetchingXdcToken(state, pid, eid, xdcid),
  () => getXdcToken(pid, eid, xdcid),
);

const deleteExpXdcResponse = (resp, path) => (dispatch) => {
  const [,, pid, , eid, , xdcid] = path.split('/');
  if (resp.ok) {
    dispatch(getExpXdcs(pid, eid)); // get updated information after delete.
    return dispatch(ExpXdcDelete(pid, eid, xdcid)); // remove from local store.
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error deleting exp xdc ${pid}/${eid}/${xdcid} : ${resp.statusText}`));
};

const deleteExpXdc = (pid, eid, xdcid) => doFetchDelete(
  `/projects/${pid}/experiments/${eid}/xdc/${xdcid}`,
  {},
  deleteExpXdcResponse,
);

const getPublicKeys = (uid) => fetchThing(
  `users/${uid}/keys`,
  (keys) => PublicKeysReceieve(uid, 'keys', keys),
);

const fetchPublicKeysIfNeeded = (uid) => fetchThingIfNeeded(
  (state) => selectors.getPublicKeys(state, uid),
  (state) => selectors.isFetchingPublicKeys(state, uid),
  () => getPublicKeys(uid),
);

const putPubKeyResponse = (resp, path) => (dispatch) => {
  const [,, uid, ,] = path.split('/'); // eslint-disable-line comma-spacing
  if (resp.ok) {
    return dispatch(getPublicKeys(uid)); // reload keys
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error updating public keys for ${uid} : ${resp.statusText}`));
};

const putPublicKey = (uid, key) => doFetchPut(
  `/users/${uid}/keys`,
  {
    key,
  },
  putPubKeyResponse,
);

const deletePubKeyResponse = (resp, path) => (dispatch) => {
  // users/tommy/keys/09:4...
  const [, uid,, fingerprint] = path.split('/');
  if (resp.ok) {
    dispatch(PublicKeyDelete(uid, fingerprint)); // remove from local store.
    return dispatch(getPublicKeys(uid));
  }
  return dispatch(nas.SetNotifcation(nas.Level.ERROR,
    `Error deleting pubkey : ${resp.statusText}`));
};

const deletePublicKey = (uid, fingerprint) => doFetchDelete(
  `users/${uid}/keys/${fingerprint}`,
  {},
  deletePubKeyResponse,
);

const fetchTopology = (modelContents) => {
  const apiEndpoint = getAPIEndpoint();
  const url = `https://${apiEndpoint}/model/compile`;
  const token = Auth.getAccessToken();
  const opts = {
    method: 'put',
    headers: new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    }),
    body: JSON.stringify({
      model: modelContents,
      type: 'mx',
    }),
  };
  return fetch(url, opts);
};

const createExperimentPromise = (pid, eid) => {
  const apiEndpoint = getAPIEndpoint();
  const url = `https://${apiEndpoint}/projects/${pid}/experiments/${eid}`;
  const token = Auth.getAccessToken();
  const opts = {
    method: 'put',
    headers: new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    }),
    body: JSON.stringify({
      name: eid,
    }),
  };
  return fetch(url, opts);
};

export {
  fetchUserIfNeeded as fetchUser,
  fetchUsersIfNeeded as fetchUsers,
  fetchHealthIfNeeded as fetchHealth,
  fetchProjectIfNeeded as fetchProject,
  fetchProjectsIfNeeded as fetchProjects,
  fetchExperimentIfNeeded as fetchExperiment,
  fetchExperimentsIfNeeded as fetchExperiments,
  fetchMembersIfNeeded as fetchMembers,
  fetchRealizationsIfNeeded as fetchRealizations,
  fetchRealizationIfNeeded as fetchRealization,

  fetchTopology,

  putProject as createProject,
  postProject as updateProject,
  deleteProject,
  putProjectMember as addProjectMember,
  deleteProjectMember as rmProjectMember,

  putExperiment as createExperiment,
  createExperimentPromise,
  postExperiment as updateExperiment,
  deleteExperiment,

  postExpVersion as createExpVersion,
  fetchExperimentVersionsIfNeeded as fetchExperimentVersions,
  fetchExperimentVersionIfNeeded as fetchExperimentVersion,

  fetchPublicKeysIfNeeded as fetchPublicKeys,
  putPublicKey as addPublicKey,
  deletePublicKey,

  putExpXdc as spawnExpXdc,
  loadingExpXdc,
  fetchExpXdcsIfNeeded as fetchExpXdcs,
  deleteExpXdc,
  fetchXdcTokenIfNeeded as fetchXdcToken,

  putRealization as createRealization,
  deleteRealization,
  realizationAcceptAction,

  putMaterialization as createMaterialization,
  fetchMaterializationIfNeeded,
  fetchMaterialization,
  fetchMaterializationStatus,
  deleteMaterialization as dematerialize,
  MaterializationTransition,

  initUser,
  putUser as createUser,
  deleteUser,

  setAPIEndpoint,
  getAPIEndpoint,
};
