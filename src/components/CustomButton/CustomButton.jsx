import React from 'react';
import { Button, Tooltip, OverlayTrigger } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { FaPlus, FaTimes } from 'react-icons/fa';

const AddDeleteItemButton = (props) => {
  const tt = (
    <Tooltip>
      {props.add ? 'Create' : 'Delete'}
      {' '}
      {props.item}
    </Tooltip>
  );
  return (
    <OverlayTrigger placement="top" overlay={tt}>
      {props.to
        ? (
          <NavLink
            to={props.to}
          >
            {props.add ? <FaPlus /> : <FaTimes />}
          </NavLink>
        )
        : (
          <Button
            variant={props.add ? 'outline-success' : 'outline-danger'}
            size="sm"
            className="mtb_table_btn"
            onClick={props.onClick}
          >
            {props.add ? <FaPlus /> : <FaTimes />}
          </Button>
        )}
    </OverlayTrigger>
  );
};

export default AddDeleteItemButton;
