import React, { Component } from 'react';
import {
  Button, Container, Card, Row, Col,
} from 'react-bootstrap';
import { connect } from 'react-redux';
import AuthUserCard from 'components/UserCard/AuthUserCard';
import * as AuthActions from 'actions/auth_actions';
import * as TbapiActions from 'actions/tbapi_actions';
import * as selectors from 'reducers/selectors';
import Loading from 'components/Dialog/Loading';

class AuthStatus extends Component {
  constructor(props) {
    super(props);
    this.doAuthentication = this.doAuthentication.bind(this);
    this.refreshData = this.refreshData.bind(this);
    this.doLogout = this.doLogout.bind(this);
    this.state = {
      loading: false,
      loaded: false,
    };
  }

  componentDidMount() {
    this.refreshData();
  }

  componentDidUpdate() {
    const { profile } = this.props;
    if (profile && this.state.loading) {
      if (profile) { // profile finished loading.
        this.setState({ // eslint-disable-line react/no-did-update-set-state
          loading: false,
          loaded: true,
        });
      } else {
        this.setState({ // eslint-disable-line react/no-did-update-set-state
          loaded: false,
          loading: false,
        });
      }
    }
  }

  refreshData() {
    const { isAuthenticated, profile } = this.props;
    if (isAuthenticated) {
      if (!profile) {
        if (!this.state.loading) {
          this.setState({ loading: true, loaded: false });
          this.props.getAuthProfile();
          this.props.initUser();
        }
      }
    }
  }

  doAuthentication() {
    this.props.authLogin();
  }

  doLogout() {
    this.setState({
      loading: false,
      loaded: false,
    });
    this.props.authLogout();
  }

  render() {
    const { isAuthenticated, profile } = this.props;
    let infoStr = '';

    if (!isAuthenticated) {
      infoStr = 'Not authenticated.';
    } else if (this.state.loaded) {
      infoStr = `Authenticated as ${profile.email}. `;
    } else {
      infoStr = 'Loading Authentication Profile...';
    }
    return (
      <Card>
        <Card.Header>Authentication Status</Card.Header>
        <Card.Body>
          <Container fluid>
            <Row>
              <Col>
                { this.state.loading && <Loading /> }
                { this.state.loaded && <AuthUserCard /> }
              </Col>
            </Row>
            <Row>
              <Col md={{ span: 3, offset: 9 }}>
                { !isAuthenticated ? (
                  <Button onClick={this.doAuthentication} variant="success">
                    Authenticate
                  </Button>
                ) : (
                  <Button onClick={this.doLogout} variant="danger">
                    Logout
                  </Button>
                )}
              </Col>
            </Row>
          </Container>
        </Card.Body>
        <Card.Footer>
          <small className="itext-muted">{infoStr}</small>
        </Card.Footer>
      </Card>
    );
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: selectors.getIsAuthenticated(state),
  profile: selectors.getAuthdProfile(state),
});

const mapDispatchToProps = (dispatch) => ({
  authLogout: () => dispatch(AuthActions.logout()),
  authLogin: () => dispatch(AuthActions.login()),
  getAuthProfile: () => dispatch(AuthActions.getProfile()),
  initUser: () => dispatch(TbapiActions.initUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthStatus);
