import React, { Component } from 'react';
import {
  Card, FormGroup, Form, FormControl, Button,
} from 'react-bootstrap';
import * as TbapiActions from 'actions/tbapi_actions';
import * as selectors from 'reducers/selectors';
import { connect } from 'react-redux';

class TBAPIEndpointConfiguration extends Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.setAPIEndpoint = this.setAPIEndpoint.bind(this);
    this.state = {
      endpoint: TbapiActions.getAPIEndpoint(),
    };
  }

  componentDidMount() {
    this.props.apiCheck();
  }

  componentDidUpdate() {
    this.props.apiCheck();
  }

  setAPIEndpoint() {
    TbapiActions.setAPIEndpoint(this.state.endpoint);
    this.props.apiCheck();
  }

  handleInputChange(event) {
    if (event.target.name === 'endpoint') {
      this.setState({
        endpoint: event.target.value,
      });
    }
  }

  render() {
    const { apiEndpointActive, isAuthenticated } = this.props;
    let infoStr = '';
    if (!isAuthenticated) {
      infoStr = 'Authenticate to access API endpoint.';
    } else if (apiEndpointActive) {
      infoStr = 'API endpoint is active.';
    } else if (this.state.endpoint) {
      infoStr = 'Unable to connect to API endpoint.';
    } else {
      infoStr = 'API endpoint is not configured.';
    }
    return (
      <Card>
        <Card.Header>Testbed API Endpoint Configuration</Card.Header>
        <Card.Body>
          <Form>
            <FormGroup>
              <Form.Label>Address</Form.Label>
              <FormControl
                name="endpoint"
                type="text"
                onChange={this.handleInputChange}
                bsPrefix="form-control"
                value={this.state.endpoint}
              />
            </FormGroup>
            <Button className="float-right" variant="outline-primary" onClick={this.setAPIEndpoint}>
              Set Testbed API Endpoint
            </Button>
          </Form>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">{infoStr}</small>
        </Card.Footer>
      </Card>
    );
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: selectors.getIsAuthenticated(state),
  apiEndpointActive: selectors.getHealth(state),
});

const mapDispatchToProps = (dispatch) => ({
  apiCheck: () => dispatch(TbapiActions.fetchHealth()),
});

export default connect(mapStateToProps, mapDispatchToProps)(TBAPIEndpointConfiguration);
