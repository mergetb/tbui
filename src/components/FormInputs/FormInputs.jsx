import React, { Component } from 'react';
import {
  FormGroup, Form, FormControl, Row,
} from 'react-bootstrap';

function FieldGroup({ label, ...props }) {
  return (
    <FormGroup>
      <Form.Label>{label}</Form.Label>
      <FormControl {...props} />
    </FormGroup>
  );
}

class FormInputs extends Component {
  render() {
    const row = [];
    for (let i = 0; i < this.props.ncols.length; i += 1) {
      row.push(
        <div key={i} className={this.props.ncols[i]}>
          <FieldGroup {...this.props.proprieties[i]} />
        </div>,
      );
    }
    return <Row>{row}</Row>;
  }
}

export default FormInputs;
