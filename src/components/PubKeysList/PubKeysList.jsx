import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Card, Container, Row, Table, Col,
} from 'react-bootstrap';
import * as nas from 'actions/notification_actions.js';
import LoadingCard from 'components/Dialog/LoadingCard.jsx';
import AddPubKeyDialog from 'components/Dialog/AddPubKeyDialog.jsx';
import AddDeleteItemButton from 'components/CustomButton/CustomButton';

import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';

class PubKeysList extends Component {
  constructor(props) {
    super(props);
    this.getData = this.getData.bind(this);
    this.state = { modalOpen: false };
    this.handleAdd = this.handleAdd.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  componentDidUpdate() {
    this.getData();
  }

  onCloseModal() {
    this.setState({
      modalOpen: false,
    });
  }

  getData() {
    const { profile, fetchPublicKeys } = this.props;
    if (profile) {
      fetchPublicKeys(profile.nickname);
    }
  }

  handleDelete(fingerprint) {
    const { profile, deletePubKey, notify } = this.props;
    if (profile) {
      deletePubKey(profile.nickname, fingerprint);
      notify(nas.Level.INFO, `Deleted public key ${fingerprint}`);
    }
  }

  showModal() {
    this.setState({
      modalOpen: true,
    });
  }

  handleAdd() {
    this.onCloseModal();
    this.getData();
  }

  render() {
    const { pubkeys, profile } = this.props;

    if (!pubkeys) {
      return (
        <div className="content">
          <Container fluid>
            <Row>
              <LoadingCard
                title="Public Keys"
                category={profile && `associated with ${profile.nickname}`}
              />
            </Row>
          </Container>
        </div>
      );
    }

    return (
      <>
        <AddPubKeyDialog
          username={profile.nickname}
          show={this.state.modalOpen}
          onSubmit={this.handleAdd}
          onClose={this.onCloseModal}
        />
        <Container fluid>
          <Row>
            <Col md={12}>
              <Card>
                <Card.Header>{`Public Keys for ${profile.nickname}`}</Card.Header>
                <Card.Body>
                  <Table striped hover>
                    <thead>
                      <tr>
                        <th>Fingerprint</th>
                        <th>Key</th>
                        <th>{ }</th>
                      </tr>
                    </thead>
                    <tbody>
                      {pubkeys.map((pkey) => (
                        <tr key={pkey.key.substring(0, 16)}>
                          <td key={pkey.fingerprint}>
                            {pkey.fingerprint}
                          </td>
                          <td key={pkey.key.substring(0, 16)}>
                            {`${pkey.key.substring(0, 16)}...`}
                          </td>
                          <td className="tc-actions text-right">
                            <AddDeleteItemButton
                              add={false}
                              item="Public Key"
                              onClick={() => this.handleDelete(pkey.fingerprint)}
                            />
                          </td>
                        </tr>
                      ))}
                      <tr>
                        <td className="tc-actions text-left">
                          <AddDeleteItemButton
                            add
                            item="Public Key"
                            onClick={() => this.showModal()}
                          />
                        </td>
                        <td />
                        <td />
                      </tr>
                    </tbody>
                  </Table>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { profile } = props;
  let nickname = null;
  if (profile) {
    nickname = profile.nickname;
  }
  return {
    pubkeys: selectors.getPublicKeys(state, nickname),
    profile: selectors.getAuthdProfile(state),
  };
};

const mapDispatchToProps = (dispatch) => ({
  fetchPublicKeys: (user) => dispatch(TbapiActions.fetchPublicKeys(user)),
  deletePubKey: (user, fingerprint) => dispatch(TbapiActions.deletePublicKey(user, fingerprint)),
  notify: (level, message) => dispatch(nas.SetNotifcation(level, message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PubKeysList);
