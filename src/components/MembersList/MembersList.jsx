import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Card, Table } from 'react-bootstrap';

import Loading from 'components/Dialog/Loading';
import AddDeleteItemButton from 'components/CustomButton/CustomButton';

import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';

class MembersList extends Component {
  constructor(props) {
    super(props);
    this.getMembersDataIfNeeded = this.getMembersDataIfNeeded.bind(this);
  }

  componentDidMount() {
    this.getMembersDataIfNeeded();
  }

  componentDidUpdate() {
    this.getMembersDataIfNeeded();
  }

  getMembersDataIfNeeded() {
    const { isAuthenticated, dispatch } = this.props;
    if (isAuthenticated) {
      const { projectname } = this.props.match.params;
      dispatch(TbapiActions.fetchMembers(projectname));
    }
  }

  handleDelete(name) {
    const { projectname } = this.props.match.params;
    this.props.dispatch(TbapiActions.rmProjectMember(projectname, name));
  }

  render() {
    const { members, fetching } = this.props;
    const { projectname } = this.props.match.params;

    const title = 'Members';
    const subtitle = `users in ${projectname}`;

    if (!members || fetching) {
      return (
        <Card key={0} title={title} category={subtitle} content={<Loading />} />
      );
    }

    return (
      <Card>
        <Card.Header>{title}</Card.Header>
        <Card.Body>
          <Table striped hover>
            <thead>
              <tr>
                <th>Name</th>
                <th>Role</th>
                <th>State</th>
                <th>{ }</th>
              </tr>
            </thead>
            <tbody>
              {Object.keys(members).map((name) => (
                <tr key={name}>
                  <td key={`${name}.1`}>
                    <NavLink to={`/users/${name}`}>
                      {name}
                    </NavLink>
                  </td>
                  <td key={`${name}.2`}>
                    {members[name].role}
                  </td>
                  <td key={`${name}.3`}>
                    {members[name].state}
                  </td>
                  <td className="tc-actions text-right">
                    <AddDeleteItemButton add={false} item="Member" onClick={() => this.handleDelete(name)} />
                  </td>
                </tr>
              ))}
              <tr>
                <td className="tc-actions text-left">
                  <AddDeleteItemButton
                    add
                    item="Member"
                    to={`/create/projectmember/${projectname}`}
                  />
                </td>
                <td />
                <td />
                <td />
              </tr>
            </tbody>
          </Table>
        </Card.Body>
      </Card>
    );
  }
}

const mapStateToProps = (state, props) => ({
  isAuthenticated: selectors.getIsAuthenticated(state),
  members: selectors.getMembers(state, props.match.params.projectname),
  fetching: selectors.isFetchingMembers(state, props.match.params.projectname),
});

export default connect(mapStateToProps)(MembersList);
