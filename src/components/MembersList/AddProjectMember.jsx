import React, { Component } from 'react';
import {
  Button, Card, FormGroup, FormControl, Form,
} from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';
import * as nas from 'actions/notification_actions.js';
import CustomAlert from 'components/Dialog/CustomAlert';

class AddProjectMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: null,
      checkinguser: false,
      uservalid: false,
      error: false,
    };
    this.toggleError = this.toggleError.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
  }

  componentDidUpdate() {
    const { checkinguser } = this.state;

    if (checkinguser) {
      const users = this.props.getUsers();

      if (users) {
        const valid = this.props.getUser(this.state.username);
        if (valid) {
          const { projectname } = this.props.match.params;
          this.props.putMember(projectname, this.state.username);
          this.setState({ // eslint-disable-line react/no-did-update-set-state
            checkinguser: false,
            uservalid: true,
            error: false,
          });
        } else {
          this.setState({ // eslint-disable-line react/no-did-update-set-state
            checkinguser: false,
            uservalid: false,
            error: true,
          });
        }
      }
    }
  }

  handleNameChange(event) {
    this.setState({
      username: event.target.value,
    });
  }

  handleClick() {
    const { fetchUser } = this.props;

    fetchUser(this.state.username);

    this.setState({
      checkinguser: true,
    });
  }

  toggleError() {
    this.setState((prevState) => ({ error: !prevState.error }));
  }

  render() {
    const { projectname } = this.props.match.params;
    const { uservalid, checkinguser } = this.state;

    if (uservalid) {
      return <Redirect to={`/projects/${projectname}`} />;
    }

    if (checkinguser) {
      return (
        <Card>
          <Card.Title>
            Validating User
          </Card.Title>
          <Card.Body>
            {`Validating user ${this.state.username}`}
          </Card.Body>
        </Card>
      );
    }

    return (
      <>
        <CustomAlert
          show={this.state.error}
          alertstyle="danger"
          onClose={this.toggleError}
          header="Unknown User"
          message="Please choose an existing user"
        />
        <Card>
          <Card.Header>
            {`Add Member to Project ${projectname}`}
          </Card.Header>
          <Card.Body>
            <form>
              <FormGroup>
                <Form.Label>Username</Form.Label>
                <FormControl
                  id="nameForm"
                  type="text"
                  placeholder="Username"
                  onChange={this.handleNameChange}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') this.handleClick();
                  }}
                />
              </FormGroup>
              <Button
                variant="info"
                disabled={checkinguser}
                onClick={this.handleClick}
              >
                Add Member
              </Button>
            </form>
          </Card.Body>
        </Card>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  getUser: (uid) => selectors.getUser(state, uid),
  getUsers: () => selectors.getUsers(state),
});

const mapDispatchToProps = (dispatch) => ({
  fetchUser: (uid) => dispatch(TbapiActions.fetchUser(uid)),
  putMember: (pid, uid) => dispatch(TbapiActions.addProjectMember(pid, uid)),
  nasError: (msg) => dispatch(nas.SetNotifcation(nas.Level.ERROR, msg)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddProjectMember);
