import React, { Component } from 'react';
import {
  Card, Row, Col,
} from 'react-bootstrap';

class MergeError extends Component {
  render() {
    const { mergeError, parsedEvidence } = this.props;
    const {
      title, detail, type, evidence,
    } = mergeError;

    const hdr = 'MergeError: '.concat(title);

    return (
      <Card border="danger">
        <Card.Header>{hdr}</Card.Header>
        <Card.Body>
          <Row>
            <Col xs={3}><Card.Text>Detail</Card.Text></Col>
            <Col><Card.Text>{detail}</Card.Text></Col>
          </Row>
          <Row>
            <Col xs={3}><Card.Text>Information</Card.Text></Col>
            <Col><Card.Link href={type}>{type}</Card.Link></Col>
          </Row>
          <Row>
            <Col xs={3}><Card.Text>Evidence</Card.Text></Col>
            {
              parsedEvidence
                ? (
                  <Col><Card.Text>{parsedEvidence}</Card.Text></Col>
                )
                : (
                  <Col><Card.Text>{evidence}</Card.Text></Col>
                )
            }
          </Row>
        </Card.Body>
      </Card>
    );
  }
}

export default MergeError;
