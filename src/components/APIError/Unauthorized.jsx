import React from 'react';
import { Card } from 'react-bootstrap';

const Unauthorized = () => (
  <Card>
    <Card.Title>
      Unauthorized
    </Card.Title>
    <Card.Body>
      Sorry, that page was not found.
    </Card.Body>
  </Card>
);

export default Unauthorized;
