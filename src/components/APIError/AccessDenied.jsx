import React from 'react';
import { Card, Jumbotron } from 'react-bootstrap';

const AccessDenied = () => (
  <Card>
    <Card.Title>
      Access Denied
    </Card.Title>
    <Jumbotron>
      A Merge API action was denied by policy.
    </Jumbotron>
  </Card>
);

export default AccessDenied;
