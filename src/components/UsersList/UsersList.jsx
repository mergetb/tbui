import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Card, Table } from 'react-bootstrap';
import LoadingCard from 'components/Dialog/LoadingCard';

import * as TbapiActions from 'actions/tbapi_actions';
import * as selectors from 'reducers/selectors';

class UsersList extends Component {
  constructor(props) {
    super(props);
    this.getData = this.getData.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  componentDidUpdate() {
    this.getData();
  }

  getData() {
    const { fetchUsers } = this.props;
    fetchUsers();
  }

  render() {
    const { users } = this.props;

    if (!users) {
      return <LoadingCard title="Users" />;
    }

    return (
      <Card>
        <Card.Header>Users</Card.Header>
        <Card.Body>
          <Table striped hover>
            <thead>
              <tr>
                <th>Username</th>
                <th>Full Name</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              {Object.keys(users).map((user) => (
                <tr key={user}>
                  <td key={`${user}.1`}>
                    <NavLink to={`users/${users[user].username}`}>
                      {users[user].username}
                    </NavLink>
                  </td>
                  <td key={`${user}.2`}>{users[user].name}</td>
                  <td key={`${user}.3`}>
                    <a href={`mailto:${users[user].email}`}>
                      {users[user].email}
                    </a>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card.Body>
      </Card>
    );
  }
}

const mapStateToProps = (state) => ({
  users: selectors.getUsers(state),
});

const mapDispatchToProps = (dispatch) => ({
  fetchUsers: () => { dispatch(TbapiActions.fetchUsers()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
