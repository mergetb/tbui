import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import { Redirect, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import * as selectors from 'reducers/selectors';
import * as AuthActions from 'actions/auth_actions.js';

class Landing extends Component {
  render() {
    if (this.props.authd && this.props.userActive) {
      return <Redirect to="/" />;
    }

    let cardText = (
      <Card.Text>
        Please
        {' '}
        <NavLink to="#" onClick={this.props.login}>login</NavLink>
        {' '}
        to access your testbed.
      </Card.Text>
    );

    if (this.props.expired) {
      cardText = (
        <Card.Text>
          Authentication token expired. Please
          {' '}
          <NavLink to="#" onClick={this.props.login}>login</NavLink>
          {' '}
          again to access your testbed.
        </Card.Text>
      );
    }

    if (this.props.userActive === false) {
      cardText = (
        <Card.Text>
          Your account is not active. Please ask testbed operations to activate your account.
        </Card.Text>
      );
    }

    return (
      <Card>
        <Card.Header>Welcome to MergeTB</Card.Header>
        <Card.Body>
          {cardText}
        </Card.Body>
      </Card>
    );
  }
}

const mapStateToProps = (state) => ({
  authd: selectors.getIsAuthenticated(state),
  expired: selectors.getIsTokenExpired(state),
  userActive: selectors.getUserActive(state),
});

const mapDispatchToProps = (dispatch) => ({
  login: () => dispatch(AuthActions.login()),
  logout: () => dispatch(AuthActions.logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
