import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Navbar } from 'react-bootstrap';
import logo from 'assets/img/merge-light.svg';
import * as TbapiActions from 'actions/tbapi_actions';
import HeaderLinks from './HeaderLinks.jsx';

class Header extends Component {
  componentDidMount() {
    this.props.initUser();
  }

  render() {
    return (
      <Navbar bg="light" variant="light" sticky="top">
        <Navbar.Brand href="https://launch.mergetb.net">
          <span>
            <img
              src={logo}
              width="30"
              height="30"
              className="d-inline-block align-top"
              alt="MergeTB"
            />
            {'   MergeTB'}
          </span>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <HeaderLinks {...this.props} />
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

const mapStateToProps = (state) => { return {}; }; // eslint-disable-line

const mapDispatchToProps = (dispatch) => ({
  initUser: () => dispatch(TbapiActions.initUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
