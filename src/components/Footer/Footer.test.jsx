import React from 'react';
import renderer from 'react-test-renderer';
import Footer from './Footer';

// it('Footer renders without crashing', () => {
//   const div = document.createElement('div');
//   ReactDOM.render(<Footer />, div);
//   ReactDOM.unmountComponentAtNode(div);
// });

it('Footer snapshot', () => {
  const tree = renderer.create(<Footer />).toJSON();
  expect(tree).toMatchSnapshot();
});
