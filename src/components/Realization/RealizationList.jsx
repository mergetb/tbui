import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, NavLink } from 'react-router-dom';
import { Card, Table } from 'react-bootstrap';
import * as nas from 'actions/notification_actions.js';
import LoadingCard from 'components/Dialog/LoadingCard';
import AddDeleteItemButton from 'components/CustomButton/CustomButton';

import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';

class RealizationList extends Component {
  constructor(props) {
    super(props);
    this.getDataIfNeeded = this.getDataIfNeeded.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidMount() {
    this.getDataIfNeeded();
  }

  componentDidUpdate() {
    this.getDataIfNeeded();
  }

  getDataIfNeeded() {
    const { authd, dispatch } = this.props;
    const { projectname, experimentname } = this.props.match.params;
    if (authd) {
      dispatch(TbapiActions.fetchRealizations(projectname, experimentname));
    }
  }

  handleDelete(pid, eid, rid) {
    const { dispatch } = this.props;
    dispatch(TbapiActions.deleteRealization(pid, eid, rid));
    dispatch(nas.SetNotifcation(
      nas.Level.INFO, `Deleted realization ${pid}/${eid}/${rid}`,
    ));
  }

  render() {
    const { reals, fetching, authd } = this.props;
    const { projectname, experimentname } = this.props.match.params;

    if (!authd) {
      return <Redirect to="/landing" />;
    }

    const title = 'Realizations';
    const subtitle = `for ${projectname}, ${experimentname}`;

    if (!reals || fetching) {
      return (
        <LoadingCard title={title} subtitle={subtitle} />
      );
    }

    let body = (<Card.Text>No realizations</Card.Text>);

    if (reals.length !== 0) {
      body = (
        <Table striped hover>
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {reals.map((name) => (
              <tr key={name}>
                <td key={`${name}.1`}>
                  <NavLink to={`/projects/${projectname}/experiments/${experimentname
                  }/realizations/${name}`}
                  >
                    {name}
                  </NavLink>
                </td>
                <td className="tc-actions text-right">
                  <AddDeleteItemButton
                    add={false}
                    item="Realization"
                    onClick={() => this.handleDelete(projectname, experimentname, name)}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      );
    }

    return (
      <Card>
        <Card.Header>{title}</Card.Header>
        <Card.Body>
          {body}
        </Card.Body>
      </Card>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { projectname, experimentname } = props.match.params;
  return {
    authd: selectors.getIsAuthenticated(state),
    reals: selectors.getRealizations(state, projectname, experimentname),
    fetching: selectors.isFetchingRealizations(state, projectname, experimentname),
  };
};

export default connect(mapStateToProps)(RealizationList);
