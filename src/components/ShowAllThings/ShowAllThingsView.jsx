import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as selectors from 'reducers/selectors.js';
import Projects from 'components/ShowAllThings/AllThings';
import { Card } from 'react-bootstrap';

class ShowAllThings extends Component {
  render() {
    if (!this.props.authd || !this.props.userActive) {
      return <Redirect to="/landing" />;
    }
    return (
      <Card>
        <Card.Header>
          Your Testbed
        </Card.Header>
        <Card.Body>
          <Projects />
        </Card.Body>
      </Card>
    );
  }
}

const mapStateToProps = (state) => ({
  authd: selectors.getIsAuthenticated(state),
  userActive: selectors.getUserActive(state),
});

export default connect(mapStateToProps)(ShowAllThings);
