/* eslint-disable max-classes-per-file */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as selectors from 'reducers/selectors.js';
import * as TbapiActions from 'actions/tbapi_actions.js';
import { Tooltip, OverlayTrigger, Table } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import Loading from 'components/Dialog/Loading';

class RealCellConnected extends Component {
  componentDidMount() {
    this.props.fetchReals();
  }

  render() {
    const { reals, pid, eid } = this.props;

    // realizations.
    if (!reals) {
      return <Loading />;
    } if (Object.keys(reals).length === 0) {
      return '';
    }
    const cells = [];
    for (let i = 0; i < reals.length; i += 1) {
      cells.push(
        <li key={i}>
          <NavLink to={`/projects/${pid}/experiments/${eid}/realizations/${reals[i]}`}>
            {reals[i]}
          </NavLink>
        </li>,
      );
    }
    return <ul className="mtb_yourtestbed">{cells}</ul>;
  }
}

const RealCell = connect(
  (state, props) => ({
    reals: selectors.getRealizations(state, props.pid, props.eid),
  }),
  (dispatch, props) => ({
    fetchReals: () => dispatch(TbapiActions.fetchRealizations(props.pid, props.eid)),
  }),
)(RealCellConnected);

class XDCCellConnected extends Component {
  componentDidMount() {
    this.props.fetchXdcs();
  }

  componentDidUpdate() {
    // load tokens once we've got the xdc names. when/if they show up then use them.
    const { xdcs } = this.props;
    if (xdcs) {
      for (let i = 0; i < xdcs.length; i += 1) {
        this.props.fetchToken(xdcs[i].name);
      }
    }
  }

  render() {
    const { xdcs, tokens } = this.props;

    if (!xdcs) {
      return <Loading />;
    } if (Object.keys(xdcs).length === 0) {
      return '';
    }
    const xdclist = [];
    for (let i = 0; i < xdcs.length; i += 1) {
      if (tokens) {
        if (tokens[xdcs[i].name]) {
          // we have a matching token for this xdc, so craft a link to it.
          xdclist.push(
            <li key={i}>
              <a href={`${xdcs[i].link}/?token=${tokens[xdcs[i].name]}`} rel="noopener noreferrer" target="_blank">
                {xdcs[i].name}
              </a>
            </li>,
          );
        }
      } else {
        xdclist.push(<li key={i}>{xdcs[i].name}</li>);
      }
    }
    return <ul className="mtb_yourtestbed">{xdclist}</ul>;
  }
}
const XDCCell = connect(
  (state, props) => ({
    xdcs: selectors.getExpXdcs(state, props.pid, props.eid),
    tokens: selectors.getXdcTokens(state, props.pid, props.eid),
  }),
  (dispatch, props) => ({
    fetchToken: (xdcid) => dispatch(TbapiActions.fetchXdcToken(props.pid, props.eid, xdcid)),
    fetchXdcs: () => dispatch(TbapiActions.fetchExpXdcs(props.pid, props.eid)),
  }),
)(XDCCellConnected);

// handle loading experiment version in sorted date order.
class ExpVersionCellConnected extends Component {
  componentDidMount() {
    this.props.fetchVersions();
  }

  componentDidUpdate() {
    // If we have the hashes of the versions, fetch the info for each.
    const { vers, fetchVersion } = this.props;
    if (vers && vers.length !== 0) {
      for (let i = 0; i < vers.length; i += 1) {
        // if we don't have it, fetch it.
        if (!this.props.getVersion(vers[i])) {
          fetchVersion(vers[i]);
        }
      }
    }
  }

  render() {
    const { pid, eid, vers } = this.props;
    // vers is an array of hashes.
    if (!vers) {
      return <Loading />;
    } if (Object.keys(vers).length === 0) {
      return '';
    }
    // we have hashes, using them select the info associated with them.
    const verinfos = [];
    for (let i = 0; i < vers.length; i += 1) {
      const info = this.props.getVersion(vers[i]); // do we actually have the data yet?
      if (info) {
        info.hash = vers[i]; // the hash is not in the info! keep track so we can use it later.
        verinfos.push(info);
      }
    }
    // if the lenght of verinfos is shorter than vers, we've not gotten all the data,
    // but as some point we will...

    // Now that we have the list version infos, sort by date and limit to 3.
    if (verinfos.length !== 0) {
      verinfos.sort((a, b) => {
        const x = new Date(a.pushDate);
        const y = new Date(b.pushDate);
        return x > y ? -1 : x < y ? 1 : 0; // eslint-disable-line no-nested-ternary
      });
      const verlist = [];
      for (let i = 0; i < verinfos.length; i += 1) {
        if (i === 3) {
          break;
        }
        verlist.push(
          <li key={i}>
            <NavLink to={`/projects/${pid}/experiments/${eid}/src/${verinfos[i].hash}`}>{verinfos[i].hash.slice(0, 8)}</NavLink>
          </li>,
        );
      }
      let classes = 'mtb_yourtestbed ';
      if (verinfos.length >= 3) {
        classes += 'mtb_partiallist';
      }
      return <ul className={classes}>{verlist}</ul>;
    }

    return <Loading />;
  }
}
const ExpVersionCell = connect(
  (state, props) => ({
    vers: selectors.getExperimentVersions(state, props.pid, props.eid),
    getVersion: (hash) => selectors.getExperimentVersion(state, props.pid, props.eid, hash),
  }),
  (dispatch, props) => ({
    fetchVersions: () => dispatch(TbapiActions.fetchExperimentVersions(props.pid, props.eid)),
    fetchVersion: (hash) => dispatch(
      TbapiActions.fetchExperimentVersion(props.pid, props.eid, hash),
    ),
  }),
)(ExpVersionCellConnected);

// Handle Experiment
class HEX extends Component {
  componentDidMount() {
    const { fetchExp, fetchRlzs } = this.props;
    fetchExp();
    fetchRlzs();
  }

  render() {
    const {
      pid, eid, exp,
    } = this.props;
    // load the four cells of the experiment <td>s based on loading status.
    // exp
    if (!exp) {
      return <Loading />;
    }
    return (
      <tr key={pid + eid}>
        {this.props.parentCells.map((c, i) => (
          // eslint-disable-next-line react/no-array-index-key
          <td key={pid + eid + i}>{c}</td>
        ))}
        <td key={pid + eid}>
          <NavLink to={`/projects/${pid}/experiments/${eid}`}>{exp.name}</NavLink>
        </td>
        <td key={`xdc${pid}${eid}`}>
          <XDCCell pid={pid} eid={eid} />
        </td>
        <td key={`expVer${pid}${eid}`}>
          <ExpVersionCell pid={pid} eid={eid} />
        </td>
        <td key={`rlz${pid}${eid}`}>
          <RealCell pid={pid} eid={eid} />
        </td>
      </tr>
    );
  }
}
const Experiment = connect(
  (state, props) => ({
    exp: selectors.getExperiment(state, props.pid, props.eid),
    rlzs: selectors.getRealizations(state, props.pid, props.eid),
  }),
  (dispatch, props) => ({
    fetchExp: () => dispatch(TbapiActions.fetchExperiment(props.pid, props.eid)),
    fetchRlzs: () => dispatch(TbapiActions.fetchRealizations(props.pid, props.eid)),
  }),
)(HEX);

// Handle Project
class HPJ extends Component {
  componentDidMount() {
    this.props.fetchProject();
    this.props.fetchExperiments();
  }

  render() {
    const {
      prj, exps, pid,
    } = this.props;
    if (!prj || !exps) {
      return <tr><td key={0}><Loading /></td></tr>;
    }
    let mode;
    switch (prj.accessMode) {
      case 'public': mode = 'text-success'; break;
      case 'protected': mode = 'text-warning'; break;
      case 'private': mode = 'text-danger'; break;
      default: mode = 'text-success'; break;
    }

    const cells = [
      <NavLink to={`/projects/${pid}`} className={mode}>{prj.name}</NavLink>,
    ];
    if (!exps || Object.keys(exps).length === 0) {
      const createTt = (
        <Tooltip id="create_tooltip">
          {' '}
          Create Experiment in
          {prj.name}
          {' '}
        </Tooltip>
      );
      return (
        <tr>
          {cells.map((c) => <td key={pid}>{c}</td>)}
          <td className="tc-actions">
            <OverlayTrigger placement="top" overlay={createTt}>
              <NavLink to={`/create/experiment/${pid}`}>
                <i className="fa fa-plus" />
              </NavLink>
            </OverlayTrigger>
          </td>
          <td />
          <td />
          <td />
        </tr>
      );
    }
    return Object.keys(exps).map((e) => (
      <Experiment
        {...this.props}
        eid={e}
        parentCells={cells}
        key={pid + e}
      />
    ));
  }
}
// GTL this is about the ugliest thing I've ever seen.
const Project = connect(
  (state, props) => ({
    prj: selectors.getProject(state, props.pid),
    exps: selectors.getExperiments(state, props.pid),
  }),
  (dispatch, props) => ({
    fetchProject: () => dispatch(TbapiActions.fetchProject(props.pid)),
    fetchExperiments: () => dispatch(TbapiActions.fetchExperiments(props.pid)),
  }),
)(HPJ);

class Projects extends Component {
  componentDidMount() {
    this.props.fetchProjects();
  }

  render() {
    const { prjs } = this.props;

    if (!prjs) {
      return <Loading />;
    }
    // this is a cheat. but I can't sort out an OOP/right way to do this at the moment.
    const headers = [
      'Project',
      'Experiment',
      'XDCs',
      'Latest Exp. Version(s)',
      'Realizations',
    ];
    const jsxHeaders = headers.map((text) => <th key={text}>{text}</th>);
    return (
      <Table striped hover bordered>
        <thead>
          <tr>
            {jsxHeaders}
          </tr>
        </thead>
        <tbody>
          {/* each of these things will create row(s): <tr>....</tr> */}
          {
            Object.keys(prjs).map((prj) => <Project key={prj} pid={prj} {...this.props} />)
          }
        </tbody>
      </Table>
    );
  }
}

export default connect(
  (state) => ({
    prjs: selectors.getProjects(state),
  }),
  (dispatch) => ({
    fetchProjects: () => dispatch(TbapiActions.fetchProjects()),
  }),
)(Projects);
