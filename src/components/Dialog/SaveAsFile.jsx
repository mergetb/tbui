import React, { Component } from 'react';
import {
  Button, Modal, Form, FormControl,
} from 'react-bootstrap';
import FileSaver from 'file-saver';

class SaveAsFile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.defaultName,
    };
    this.nameChange = this.nameChange.bind(this);
    this.handleSave = this.handleSave.bind(this);
  }

  handleSave() {
    const blob = new Blob([this.props.contents]);
    FileSaver.saveAs(blob, this.state.name);
    this.props.onClose();
  }

  nameChange(prop) {
    this.setState({ name: prop.target.value });
  }

  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.onHide}>
        <Modal.Header
          closeButton
        >
          <Modal.Title>
            Download As
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Label>Name</Form.Label>
            <FormControl
              type="text"
              label="Name"
              value={this.state.name}
              onChange={this.nameChange}
              onKeyPress={(e) => {
                if (e.key === 'Enter') this.handleLoad();
              }}
            />
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button
            onClick={this.handleSave}
            disabled={this.state.file === null}
          >
            Save
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default SaveAsFile;
