import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button, Modal, Form, FormGroup, FormControl,
} from 'react-bootstrap';
import Loading from 'components/Dialog/Loading';
import { Redirect } from 'react-router-dom';
import * as TbapiActions from 'actions/tbapi_actions.js';
import MergeError from 'components/APIError/MergeError.jsx';

function parseRlzError(err) {
  if (!Object.hasOwnProperty.call(err, 'evidence')) {
    return null;
  }
  if (!err.evidence) {
    return null;
  }

  // example data:
  // [{"Code":"PhysicallyAllocated","Constraint":"","ExpNode":"a","RlzNode":"m0","Text":""},
  // {"Code":"PhysicallyAllocated","Constraint":"","ExpNode":"a","RlzNode":"m1","Text":""},...
  const def = {};
  const notpres = {};
  const msgs = [];

  const data = JSON.parse(err.evidence);

  data.forEach((d) => {
    const { Code, ExpNode, Constraint } = d;
    switch (Code) {
      case 'ConstraintDeficient':
        switch (Constraint) {
          case 'available nodes':
            msgs.push('Too few available nodes to realize the model');
            break;
          default:
            if (ExpNode !== '' && Constraint !== '') {
              if (!def[ExpNode]) {
                def[ExpNode] = {};
              }
              if (!def[ExpNode][Constraint]) {
                def[ExpNode][Constraint] = 0;
              }
              def[ExpNode][Constraint] += 1;
            }
        }
        break;
      case 'ConstraintNotPresent':
        if (ExpNode !== '' && Constraint !== '') {
          if (!notpres[ExpNode]) {
            notpres[ExpNode] = {};
          }
          if (!notpres[ExpNode][Constraint]) {
            notpres[ExpNode][Constraint] = 0;
          }
          notpres[ExpNode][Constraint] += 1;
        }
        break;
      case 'PhysicallyAllocated':
        break;
      default:
        // NOOP
    }
  });

  /* eslint no-unused-vars: "off", no-restricted-syntax: "off" */
  for (const [node, cs] of Object.entries(def)) {
    const name = node;
    for (const [constraint, count] of Object.entries(cs)) {
      msgs.push(`Node ${node}: ${constraint} deficient ${count} times.`);
    }
  }
  for (const [node, cs] of Object.entries(notpres)) {
    const name = node;
    for (const [constraint, count] of Object.entries(cs)) {
      msgs.push(`Node ${node}: ${constraint} not present ${count} times.`);
    }
  }

  return msgs;
}


class NewRlzDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      error: false,
      realizing: false,
      rlzError: null,
      rlzErrorReason: null,
      redirect: null,
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleNameChange(event) {
    const n = event.target.value;
    const re = RegExp('^[a-z][a-z0-9]+$');
    this.setState({
      name: n,
      error: !re.test(n),
    });
  }

  handleClick() {
    const {
      pid, eid, hash, realize,
    } = this.props;

    this.setState({
      realizing: true,
      rlzError: null,
      rlzErrorReason: null,
    });

    realize(pid, eid, hash, this.state.name)
      .then((resp) => {
        if (resp.status === 200) {
          this.setState((prevState) => ({
            redirect: `/projects/${pid}/experiments/${eid}/realizations/${prevState.name}`,
          }));
        }

        // error - read the error in the body of the response
        return resp.json();
      })
      .then((err) => {
        this.setState({
          rlzError: err,
          rlzErrorReason: parseRlzError(err),
          realizing: false,
        });
      });
  }

  handleClose() {
    this.setState({
      realizing: false,
      rlzError: null,
      rlzErrorReason: null,
    });
    this.props.onClose();
  }

  render() {
    const { redirect } = this.state;
    if (redirect) {
      return <Redirect to={redirect} />;
    }

    return (
      <Modal show={this.props.show} onHide={this.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>
            {`New Realization for ${this.props.pid}/${this.props.eid}`}
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form>
            <FormGroup>
              <Form.Label>Name</Form.Label>
              <FormControl
                id="nameForm"
                type="text"
                placeholder="Name"
                onChange={this.handleNameChange}
                isInvalid={this.state.error}
                onKeyPress={(e) => {
                  if (e.key === 'Enter') this.handleClick();
                }}
              />
              <Form.Control.Feedback type="invalid">
                Realization names must be single word lowercase alphanumeric.
              </Form.Control.Feedback>
            </FormGroup>
            <Form.Group>
              <Form.Label>Experiment Version Hash</Form.Label>
              <Form.Control
                type="text"
                bsPrefix="form-control"
                readOnly
                placeholder={this.props.hash}
              />
            </Form.Group>
            {
              !this.state.realizing
                ? (
                  null
                )
                : (
                  <FormGroup>
                    <Form.Label>Realizing...</Form.Label>
                    <Loading />
                  </FormGroup>
                )
            }
          </Form>
          {
            !this.state.rlzError ? null
              : (
                <MergeError
                  mergeError={this.state.rlzError}
                  parsedEvidence={this.state.rlzErrorReason}
                />
              )
          }
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="info"
            disabled={this.state.name.length === 0}
            onClick={this.handleClick}
          >
            Realize
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
  realize: (pid, eid, hash, rid) => dispatch(TbapiActions.createRealization(pid, eid, hash, rid)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewRlzDialog);
