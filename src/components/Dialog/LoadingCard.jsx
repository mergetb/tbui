import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import Loading from 'components/Dialog/Loading';

class LoadingCard extends Component {
  render() {
    return (
      <Card>
        <Card.Header as="h4">{this.props.title}</Card.Header>
        <Card.Body>
          <Card.Text>{this.props.subtitle}</Card.Text>
          <Loading />
        </Card.Body>
      </Card>
    );
  }
}

export default LoadingCard;
