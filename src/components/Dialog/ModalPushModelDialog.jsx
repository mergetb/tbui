import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button, Modal, Form, FormGroup, Alert, FormControl,
} from 'react-bootstrap';
import { Typeahead } from 'react-bootstrap-typeahead';
import Loading from 'components/Dialog/Loading';
import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';

class ModalPushModelDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pid: null,
      eid: null,
      xir: null,
      error: null,
      expCreating: false,
      compiled: false,
    };

    this.onPidChange = this.onPidChange.bind(this);
    this.onEidChange = this.onEidChange.bind(this);
    this.onPush = this.onPush.bind(this);
  }

  loadData() { // eslint-disable-line react/sort-comp
    if (!this.state.compiled) {
      this.loadTopology();
    }
    if (!this.props.pids) {
      this.props.fetchProjects();
    }
    if (this.props.pids) {
      // pids exists.
      if (this.state.pid && !this.props.getExperiments(this.state.pid)) {
        this.props.fetchExperiments(this.state.pid);
      }
      if (!this.state.pid) {
        // set default now that it's loaded.
        this.setState({
          pid: Object.keys(this.props.pids)[0],
        });
      }
    }
  }

  componentDidMount() {
    if (this.props.show) {
      this.loadData();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.show) {
      if (prevProps.show === false) { // We are being displayed again, recompile.
        this.setState({ // eslint-disable-line react/no-did-update-set-state
          compiled: false,
          error: null,
        });
      }
      this.loadData();
    }
  }

  onPush() {
    const { pid, eid, xir } = this.state;
    const {
      pushExp, onClose, getExperiment, fetchExp, model,
    } = this.props;

    if (!getExperiment(pid, eid)) {
      this.setState({ expCreating: true });
      TbapiActions.createExperimentPromise(pid, eid)
        .then(
          (results) => {
            if (results.ok) {
              fetchExp(pid, eid); // load the data into the global state (async)
              pushExp(pid, eid, xir, model);
            }
          },
          (error) => { // eslint-disable-line no-unused-vars
            this.setState({ expCreateError: 'error' });
            return Promise.reject();
          },
        );
    } else {
      pushExp(pid, eid, xir, model);
    }
    onClose();
  }

  onPidChange(event) {
    this.setState({
      pid: event.target.value,
      eid: null,
    });
    this.typeaheadRef.clear();
  }

  onEidChange(e) {
    // slightly defensive using 3rd party input.
    if (!Array.isArray(e)) {
      return;
    }
    if (e.length <= 0) {
      return;
    }

    // is array of at least one element.
    const el = e[0];
    if (typeof el === 'string') {
      this.setState({
        eid: el,
      });
      return;
    }

    // "custom responses" from Typeahead are objects.
    if (typeof el === 'object') {
      if (Object.hasOwnProperty.call(e[0], 'customOption') && e[0].customOption === true) {
        this.setState({
          eid: e[0].label,
        });
      }
    }
  }

  loadTopology() {
    TbapiActions.fetchTopology(this.props.model)
      .then(
        (results) => {
          if (results.ok) {
            return results.json();
          }
          this.setState({
            error: 'Compile Error',
            compiled: true,
          });
          return Promise.resolve();
        },
        (error) => { // eslint-disable-line no-unused-vars
          this.setState({
            error: 'Connection Error',
          });
          return Promise.reject();
        },
      )
      .then((data) => {
        if (data) {
          this.setState({
            xir: data.xir,
            compiled: true,
          });
        }
      });
  }


  render() {
    const {
      pid, eid, xir, error, expCreateError, expCreating,
    } = this.state;
    const { pids, getExperiment } = this.props;

    let title = `Push Model to ${!pid ? '...' : pid}`;
    title += `/${!eid ? '...' : eid}`;

    // we assume the user will choose an existing exp before they choose pid and eid
    let expExists = true;
    if (pid && eid) {
      expExists = getExperiment(pid, eid); // ground truth now that we now pid and eid
    }

    let pidsUI = (<Loading />);
    if (pids) {
      pidsUI = (
        <FormControl disabled={!!this.state.error} as="select" onChange={this.onPidChange}>
          {Object.keys(pids).map((p) => (
            <option key={p} value={p}>
              {p}
            </option>
          ))}
        </FormControl>
      );
    }

    let eidsUI = (<Loading />);
    if (pid) {
      const eids = this.props.getExperiments(pid);
      if (eids) {
        eidsUI = (
          <Typeahead
            placeholder="Experiment Name..."
            disabled={!!this.state.error}
            ref={(ref) => this.typeaheadRef = ref} // eslint-disable-line no-return-assign
            id="Experiment Id"
            options={Object.keys(eids)}
            onChange={(e) => this.onEidChange(e)}
            allowNew
            newSelectionPrefix="Create a new experiment: "
            selectHintOnEnter
          />
        );
      }
    }

    let expStatus = null;
    if (expCreateError) {
      expStatus = (
        <Alert variant="danger">
          {`Error creating experiment ${eid}`}
        </Alert>
      );
    } else if (!expExists) {
      if (expCreating) {
        expStatus = (
          <Alert variant="success">
            {`Experiment ${eid} being created`}
          </Alert>
        );
      } else {
        expStatus = (
          <Alert variant="warning">
            {`Experiment ${eid} will be created on push`}
          </Alert>
        );
      }
    }

    let variant = 'warning';
    if (error) variant = 'danger';
    else if (xir) variant = 'success';

    return (
      <Modal
        show={this.props.show}
        onHide={this.props.onClose}
      >
        <Modal.Header
          closeButton
        >
          <Modal.Title>
            {title}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <FormGroup controlId="pids">
              <Form.Label>Project</Form.Label>
              {pidsUI}
            </FormGroup>
            <FormGroup controlId="eids">
              <Form.Label>Experiment</Form.Label>
              {eidsUI}
            </FormGroup>
            <Form.Label>Status</Form.Label>
            <Alert variant={variant}>
              Model Compilation:
              {' '}
              { error || (xir ? 'complete' : 'compiling') }
            </Alert>
            {expStatus}
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="info"
            disabled={!(this.state.pid && this.state.eid && this.state.xir)}
            onClick={this.onPush}
          >
            Push
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => ({
  pids: selectors.getProjects(state),
  getExperiments: (pid) => selectors.getExperiments(state, pid),
  getExperiment: (pid, eid) => selectors.getExperiment(state, pid, eid),
});

const mapDispatchToProps = (dispatch) => ({
  fetchProjects: () => dispatch(TbapiActions.fetchProjects()),
  fetchExperiments: (pid) => dispatch(TbapiActions.fetchExperiments(pid)),
  fetchExp: (pid, eid) => dispatch(TbapiActions.fetchExperiment(pid, eid)),
  pushExp: (pid, eid, xir, mdl) => dispatch(TbapiActions.createExpVersion(pid, eid, xir, mdl)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalPushModelDialog);
