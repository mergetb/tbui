import React, { Component } from 'react';
import {
  Button, Modal, Form, FormGroup, FormControl,
} from 'react-bootstrap';

const defaultModel = `import mergexp as mx

net = mx.Topology('MODELNAME')

# replace me with your experiment topology
nodes = [net.device(name) for name in ['a', 'b']]
net.connect(nodes)

mx.experiment(net)
`;

class ModelFileDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      file: null,
      buttonLabel: 'New',
    };
    this.nameChange = this.nameChange.bind(this);
    this.fileChange = this.fileChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    if (this.state.file) {
      const fileReader = new FileReader();
      fileReader.onloadend = () => {
        // Read the data, now do the callbacks.
        this.props.onSubmit(this.state.name, this.state.file.name, fileReader.result);

        this.setState({
          name: '',
          file: null,
        });
      };
      fileReader.readAsText(this.state.file);
    } else {
      // new empty "file"
      this.props.onSubmit(this.state.name, '', defaultModel.replace('MODELNAME', this.state.name));
    }

    this.props.onClose();
  }

  nameChange(e) {
    this.setState({
      name: e.target.value,
    });
  }

  fileChange(e) {
    if (this.state.name === '') {
      const fname = e.target.files[0].name;
      const n = fname.substring(0, fname.lastIndexOf('.'));
      this.setState({
        name: n,
      });
    }
    this.setState({
      file: e.target ? e.target.files[0] : null,
      buttonLabel: e.target ? 'Load' : 'New',
    });
  }

  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.onClose}>
        <Modal.Header closeButton>
          <Modal.Title>
            Load a Model File
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form>
            <FormGroup
              controlId="modelModalName"
            >
              <Form.Label>Name</Form.Label>
              <FormControl
                type="text"
                label="Model Name"
                value={this.state.name}
                onChange={this.nameChange}
                onKeyPress={(e) => {
                  if (e.key === 'Enter') this.handleClick();
                }}
              />
            </FormGroup>
            <FormGroup
              controlId="modelModalFile"
            >
              <Form.Label>File</Form.Label>
              <FormControl
                type="file"
                label="File"
                accept=".py"
                isInvalid={this.state.file === null}
                onChange={this.fileChange}
              />
            </FormGroup>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button
            onClick={this.handleClick}
            disabled={this.state.name === ''}
          >
            {this.state.buttonLabel}
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModelFileDialog;
