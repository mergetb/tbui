import React, { Component } from 'react';
import { Alert } from 'react-bootstrap';

class CustomAlert extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: this.props.show,
    };
  }

  toggleShow() {
    this.setState((prevState) => ({ show: !prevState.show }));
  }

  render() {
    if (!this.props.show) {
      return null;
    }

    const style = this.props.alertstyle ? this.props.alertstyle : 'danger';

    return (
      <Alert
        onClose={this.props.onClose}
        variant={style}
        dismissible={!!this.props.onClose}
      >
        <Alert.Heading>{this.props.header}</Alert.Heading>
        <p>{this.props.message}</p>
      </Alert>
    );
  }
}

export default CustomAlert;
