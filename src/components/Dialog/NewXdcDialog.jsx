import React, { Component } from 'react';
import {
  Button, Modal, Form, FormGroup, FormControl,
} from 'react-bootstrap';

class NewXdcDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      error: false,
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleNameChange(event) {
    const n = event.target.value;
    const re = RegExp('^[a-z][a-z0-9]+$');
    this.setState({
      name: n,
      error: !re.test(n),
    });
  }

  handleClick() {
    this.props.onSubmit(this.state.name);
    this.props.onClose();
  }

  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.onClose}>
        <Modal.Header closeButton>
          <Modal.Title>
            {`Experiment Development Container for ${this.props.pid}/${this.props.eid}`}
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form>
            <FormGroup>
              <Form.Label>Name</Form.Label>
              <FormControl
                id="nameForm"
                type="text"
                placeholder="Name"
                onChange={this.handleNameChange}
                isInvalid={this.state.error}
                onKeyPress={(e) => {
                  if (e.key === 'Enter') {
                    e.preventDefault();
                  }
                }}
              />
              <Form.Control.Feedback type="invalid">
                XDC Names must be single word lowercase alphanumeric.
              </Form.Control.Feedback>
            </FormGroup>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="info"
            disabled={this.state.error}
            onClick={this.handleClick}
          >
            Spawn XDC
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default NewXdcDialog;
