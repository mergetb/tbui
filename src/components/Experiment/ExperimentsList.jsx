import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink, Redirect } from 'react-router-dom';
import { Card, Table } from 'react-bootstrap';
import * as nas from 'actions/notification_actions.js';
import LoadingCard from 'components/Dialog/LoadingCard';
import AddDeleteItemButton from 'components/CustomButton/CustomButton';
import NewExperimentModal from 'components/Dialog/NewExperimentModal';

import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';

class ExperimentsList extends Component {
  constructor(props) {
    super(props);
    this.getExperimentsDataIfNeeded = this.getExperimentsDataIfNeeded.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.state = {
      openModal: false,
    };
  }

  componentDidMount() {
    this.getExperimentsDataIfNeeded();
  }

  componentDidUpdate() {
    this.getExperimentsDataIfNeeded();
  }

  getExperimentsDataIfNeeded() {
    const { authd, dispatch } = this.props;
    const { projectname } = this.props.match.params;
    if (authd) {
      dispatch(TbapiActions.fetchExperiments(projectname));
    }
  }

  handleDelete(projname, expname) {
    const { dispatch } = this.props;
    dispatch(TbapiActions.deleteExperiment(projname, expname));
    dispatch(nas.SetNotifcation(nas.Level.INFO, `Deleted experiment ${projname}/${expname}`));
  }

  toggleModal() {
    this.setState((prevState) => ({ openModal: !prevState.openModal }));
  }

  render() {
    const { experiments, fetching, authd } = this.props;
    const { projectname } = this.props.match.params;

    if (!authd) {
      return <Redirect to="/landing" />;
    }

    const title = 'Experiments';

    if (!experiments || fetching) {
      return (
        <LoadingCard title={title} />
      );
    }

    return (
      <>
        <NewExperimentModal
          onClose={this.toggleModal}
          show={this.state.openModal}
          pid={projectname}
          creator="You!"
        />
        <Card>
          <Card.Header>{title}</Card.Header>
          <Card.Body>
            <Table striped hover>
              <thead>
                <tr>
                  <th>Experiment Name</th>
                  <th>Description</th>
                  <th>{ }</th>
                </tr>
              </thead>
              <tbody>
                {Object.keys(experiments).map((exp) => (
                  <tr key={exp}>
                    <td key={`${exp}.1`}>
                      <NavLink to={`/projects/${projectname}/experiments/${experiments[exp].name}`}>
                        {experiments[exp].name}
                      </NavLink>
                    </td>
                    <td key={`${exp}.2`}>
                      {experiments[exp].description}
                    </td>
                    <td className="tc-actions text-right">
                      <AddDeleteItemButton
                        add={false}
                        item="Experiment"
                        onClick={() => this.handleDelete(projectname, experiments[exp].name)}
                      />
                    </td>
                  </tr>
                ))}
                <tr>
                  <td className="tc-actions text-left">
                    <AddDeleteItemButton
                      add
                      item="Experiment"
                      onClick={this.toggleModal}
                    />
                  </td>
                  <td />
                  <td />
                </tr>
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      </>
    );
  }
}

const mapStateToProps = (state, props) => ({
  authd: selectors.getIsAuthenticated(state),
  experiments: selectors.getExperiments(state, props.match.params.projectname),
  fetching: selectors.isFetchingExperiments(state, props.match.params.projectname),
});

export default connect(mapStateToProps)(ExperimentsList);
