import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Card, Table } from 'react-bootstrap';
import ExperimentVersionRow from 'components/Experiment/ExperimentVersionRow.jsx';
import Loading from 'components/Dialog/Loading';
import NewRlzDialog from 'components/Dialog/NewRlzDialog.jsx';

import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';

class ExpVersionList extends Component {
  constructor(props) {
    super(props);
    this.getVersionList = this.getVersionList.bind(this);
    this.onNewRlz = this.onNewRlz.bind(this);
    this.onCloseNewRlz = this.onCloseNewRlz.bind(this);

    this.state = {
      modalOpen: false,
      newRlzHash: null,
    };
  }

  componentDidMount() {
    this.getVersionList();
  }

  componentDidUpdate() {
    this.getVersionList();
  }

  onCloseNewRlz() {
    this.setState({
      modalOpen: false,
    });
  }

  onNewRlz(hash) {
    this.setState({
      modalOpen: true,
      newRlzHash: hash,
    });
  }

  getVersionList() {
    const { authd, dispatch } = this.props;
    const { projectname, experimentname } = this.props.match.params;
    if (authd) {
      dispatch(TbapiActions.fetchExperimentVersions(projectname, experimentname));
    }
  }

  render() {
    // Note that "versions" is just an array of hashes via /project/pid/experiments/eid/src
    // The actual data is loaded by the <ExpVersionRow ...> children.
    const { versions, authd, fetching } = this.props;
    const { projectname, experimentname } = this.props.match.params;

    if (!authd) {
      return <Redirect to="/landing" />;
    }

    const title = 'Versions';

    if (!versions || fetching) {
      return (
        <Card>
          <Card.Header>{title}</Card.Header>
          <Card.Body>
            <Loading />
          </Card.Body>
        </Card>
      );
    }
    return (
      <div>
        <NewRlzDialog
          show={this.state.modalOpen}
          onClose={this.onCloseNewRlz}
          hash={this.state.newRlzHash}
          pid={projectname}
          eid={experimentname}
        />
        <Card>
          <Card.Header>{title}</Card.Header>
          <Card.Body>
            <Table striped hover>
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Id</th>
                  <th>Node Count</th>
                  <th>Link Count</th>
                  <th>Whom</th>
                  <th>Model</th>
                  <th>Topology</th>
                  <th>{ }</th>
                </tr>
              </thead>
              <tbody>
                {versions.map((v) => (
                  <ExperimentVersionRow
                    pid={projectname}
                    eid={experimentname}
                    key={v}
                    hash={v}
                    onNewRlz={this.onNewRlz}
                  />
                ))}
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { projectname, experimentname } = props.match.params;
  return {
    authd: selectors.getIsAuthenticated(state),
    versions: selectors.getExperimentVersions(state, projectname, experimentname),
    fetching: selectors.isFetchingExperimentVersions(state, projectname, experimentname),
  };
};

export default connect(mapStateToProps)(ExpVersionList);
