import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, Table } from 'react-bootstrap';
import * as nas from 'actions/notification_actions.js';
import XdcListRow from 'components/Experiment/XdcListRow.jsx';
import LoadingCard from 'components/Dialog/LoadingCard';
import AddDeleteItemButton from 'components/CustomButton/CustomButton';
import NewXdcDialog from 'components/Dialog/NewXdcDialog.jsx';
import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';

class ExpXdcList extends Component {
  constructor(props) {
    super(props);
    this.getData = this.getData.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleNewXdc = this.handleNewXdc.bind(this);
    this.onCloseNewXdc = this.onCloseNewXdc.bind(this);

    this.state = {
      modalOpen: false,
    };
  }

  componentDidMount() {
    this.getData();
  }

  componentDidUpdate() {
    this.getData();
  }

  onCloseNewXdc() {
    this.setState({
      modalOpen: false,
    });
  }

  getData() {
    const { authd, fetchExpXdcs } = this.props;
    if (authd) {
      fetchExpXdcs();
    }
  }

  handleDelete(pid, eid, xdcid) {
    const { deleteExpXdc, notify } = this.props;
    deleteExpXdc(xdcid);
    notify(`Deleted XDC ${xdcid}`);
  }

  showNewXdc() {
    this.setState({
      modalOpen: true,
    });
  }

  handleNewXdc(name) {
    this.props.spawnXdc(name);
    this.props.loadingXdc(name); // set "fake" entry to display
    this.props.fetchExpXdcs(name); // ...and then reload them all.
    this.setState({
      modalOpen: false,
    });
  }

  render() {
    const { xdcs, fetching } = this.props;
    const { projectname, experimentname } = this.props.match.params;

    const title = 'Experiment Development Containers';

    if (!xdcs || fetching) {
      return (
        <LoadingCard title={title} />
      );
    }

    return (
      <div>
        <NewXdcDialog
          show={this.state.modalOpen}
          onSubmit={this.handleNewXdc}
          onClose={this.onCloseNewXdc}
          pid={projectname}
          eid={experimentname}
        />
        <Card>
          <Card.Header>{title}</Card.Header>
          <Card.Body>
            <Table striped hover>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Jupyter Notebook</th>
                  <th>Status</th>
                  <th>Reference Name</th>
                  <th>{ }</th>
                </tr>
              </thead>
              <tbody>
                {xdcs.map((xdc) => (
                  <XdcListRow
                    key={xdc.name}
                    pid={projectname}
                    eid={experimentname}
                    xdc={xdc}
                    delete={this.handleDelete}
                  />
                ))}
                <tr>
                  <td className="tc-actions text-left">
                    <AddDeleteItemButton
                      add
                      item="XDC"
                      onClick={() => this.showNewXdc()}
                    >
                      <i className="fa fa-plus" />
                    </AddDeleteItemButton>
                  </td>
                  <td />
                  <td />
                  <td />
                  <td />
                </tr>
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { projectname: pid, experimentname: eid } = props.match.params;
  return {
    authd: selectors.getIsAuthenticated(state),
    xdcs: selectors.getExpXdcs(state, pid, eid),
    fetching: selectors.isFetchingExpXdcs(state, pid, eid),
  };
};

const mapDispatchToProps = (dispatch, props) => {
  const { projectname: pid, experimentname: eid } = props.match.params;
  return {
    spawnXdc: (name) => dispatch(TbapiActions.spawnExpXdc(pid, eid, name)),
    loadingXdc: (name) => dispatch(TbapiActions.loadingExpXdc(pid, eid, name)),
    fetchExpXdcs: () => dispatch(TbapiActions.fetchExpXdcs(pid, eid)),
    deleteExpXdc: (xdcid) => dispatch(TbapiActions.deleteExpXdc(pid, eid, xdcid)),
    notify: (str) => dispatch(nas.SetNotifcation(nas.Level.INFO, str)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExpXdcList);
