import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loading from 'components/Dialog/Loading.jsx';
import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';
import AddDeleteItemButton from 'components/CustomButton/CustomButton';

class XdcListRow extends Component {
  constructor(props) {
    super(props);
    this.loadData = this.loadData.bind(this);
  }

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate() {
    this.loadData();
  }

  loadData() {
    const {
      pid, eid, xdc, fetchXdcToken,
    } = this.props;
    fetchXdcToken(pid, eid, xdc.name);
  }

  render() {
    // the context of this component is a row in a table. The parent understands the component
    // enough to give proper headers in the correct order. This component is bascially here
    // to load the xdc's two components as they come in: the url and the token. The url is
    // not a link if we don't have the token, and the it is a link with token appended
    // when we do get the token.
    const {
      xdc, token, pid, eid,
    } = this.props;

    let link;
    if (token) {
      link = (
        <a href={`${xdc.link}/?token=${token}`} rel="noopener noreferrer" target="_blank">
          {xdc.link}
        </a>
      );
    } else {
      link = xdc.link;
    }

    return (
      <tr>
        <td>
          {xdc.name}
        </td>
        <td>
          {link !== ''
            ? link
            : <Loading />}
        </td>
        <td>
          {token
            ? 'unlocked'
            : 'locked'}
        </td>
        <td>
          {`${xdc.name}-${eid}-${pid}`}
        </td>
        <td className="tc-actions text-right">
          <AddDeleteItemButton
            add={false}
            item="XDC"
            onClick={() => this.props.delete(pid, eid, xdc.name)}
          />
        </td>
      </tr>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { pid, eid, xdc } = props;
  return {
    token: selectors.getXdcToken(state, pid, eid, xdc.name),
  };
};

const mapDispatchToProps = (dispatch) => ({
  fetchXdcToken: (pid, eid, xdcid) => dispatch(TbapiActions.fetchXdcToken(pid, eid, xdcid)),
});

export default connect(mapStateToProps, mapDispatchToProps)(XdcListRow);
