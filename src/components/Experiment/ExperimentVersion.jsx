import React, { Component } from 'react';
import {
  Card, Row, Container, Col, Table,
} from 'react-bootstrap';
import { connect } from 'react-redux';
import * as selectors from 'reducers/selectors';
import * as TbapiActions from 'actions/tbapi_actions';
import RealizationTopology from 'components/Model/RealizationTopology';
import LoadingCard from 'components/Dialog/LoadingCard';

class ExperimentVersion extends Component {
  componentDidMount() {
    this.loadExpVer();
  }

  componentDidUpdate() {
    this.loadExpVer();
  }

  loadExpVer() {
    this.props.getExpVersion();
  }

  render() {
    const { match } = this.props;
    const { params } = match;
    const { projectname: pid, experimentname: eid, xhash } = params;
    const { expver } = this.props;

    if (!expver) {
      return <LoadingCard title="Experiment Version" />;
    }

    let title = `${pid}/${eid}`;
    if (xhash) {
      title += `: version ${xhash.slice(0, 8)}`;
    }

    return (
      <Card>
        <Card.Header>
          Experiment Version
        </Card.Header>
        <Card.Body>
          <Container fluid>
            <Row>
              <Col>
                <RealizationTopology
                  title={title}
                  topology={expver.xir}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Whom</th>
                      <th>Experiment Version Hash</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr key={1}>
                      <td key="1.1">
                        {expver.pushDate}
                      </td>
                      <td key="1.2">
                        {expver.who}
                      </td>
                      <td key="1.3">
                        {xhash}
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </Col>
            </Row>
          </Container>
        </Card.Body>
      </Card>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { match } = props;
  const { params } = match;
  const { projectname: pid, experimentname: eid, xhash } = params;

  return {
    expver: selectors.getExperimentVersion(state, pid, eid, xhash),
  };
};

const mapDispatchToProps = (dispatch, props) => {
  const { match } = props;
  const { params } = match;
  const { projectname: pid, experimentname: eid, xhash } = params;

  return {
    getExpVersion: () => dispatch(
      TbapiActions.fetchExperimentVersion(pid, eid, xhash),
    ),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExperimentVersion);
