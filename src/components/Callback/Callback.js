/* eslint-disable react/jsx-filename-extension */
import React, { Component } from 'react';
import {
  Container, Row, Col, Card,
} from 'react-bootstrap';
import { connect } from 'react-redux';
import * as authActions from 'actions/auth_actions';

class Callback extends Component {
  componentDidMount() {
    const { location, handleAuthCallback } = this.props;

    if (/access_token|id_token|error/.test(location.hash)) {
      handleAuthCallback();
    }
  }

  render() {
    const { auth } = this.props;
    const { authError } = auth;
    const message = authError || 'Loading...';

    let title;
    if (authError) {
      title = 'Error While Authenticating';
    } else {
      title = 'Authenticating...';
    }

    return (
      <Container fluid>
        <Row>
          <Col md={8}>
            <Card>
              <Card.Header>
                {title}
              </Card.Header>
              <Card.Body>
                {message}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default connect((state) => state, authActions)(Callback);
