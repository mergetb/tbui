import React, { Component } from 'react';
import {
  Card, Navbar, Nav, NavDropdown,
} from 'react-bootstrap';
import ModelVisualization from 'components/Model/ModelVisualization.jsx';
import SaveAsFile from 'components/Dialog/SaveAsFile';
import ModalPushModelDialog from 'components/Dialog/ModalPushModelDialog';

// import brace from 'brace';
import AceEditor from 'react-ace';

import 'brace/mode/python';
import 'brace/theme/github';
import 'brace/ext/language_tools'; // pulls in code for auto completion.

const themes = [
  'solarized_dark',
  'solarized_light',
  'monokai',
  'github',
  'tomorrow',
  'kuroir',
  'twilight',
  'xcode',
  'textmate',
  'terminal',
];

themes.forEach((t) => {
  require(`brace/theme/${t}`); // eslint-disable-line
});

class ModelEditor extends Component {
  constructor(props) {
    super(props);

    this.storageThemeKey = 'modelEditorTheme';
    const theme = localStorage.getItem(this.storageThemeKey);

    this.state = {
      contents: this.props.model.contents,
      visualize: false,
      width: 500,
      height: 500,
      exportFile: false,
      pushDialog: false,
      theme: theme || themes[0],
    };

    this.setDimensions = this.setDimensions.bind(this);
    this.onExport = this.onExport.bind(this);
    this.onVisualize = this.onVisualize.bind(this);
    this.onTheme = this.onTheme.bind(this);
    this.onPush = this.onPush.bind(this);
    this.onEditorChange = this.onEditorChange.bind(this);
  }

  componentDidMount() {
    this.setDimensions();
    this.saveToStorage();
    window.addEventListener('resize', this.setDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setDimensions);
  }

  onEditorChange(value) {
    this.setState({
      contents: value,
    });
    this.saveToStorage();
  }

  onExport() {
    this.setState((prevState) => ({ exportFile: !prevState.exportFile }));
  }

  onPush() {
    this.setState((prevState) => ({ pushDialog: !prevState.pushDialog }));
  }

  onTheme(t) {
    this.setState({
      theme: t,
    });
    localStorage.setItem(this.storageThemeKey, t);
  }

  onVisualize() {
    this.setState((prevState) => ({ visualize: !prevState.visualize }));
  }

  setDimensions() {
    this.setState({
      width: this.eleref.clientWidth,
      height: this.eleref.clientHeight,
    });
  }

  saveToStorage() {
    const { name, filename } = this.props.model;
    const { contents } = this.state;
    const { keyPrefix } = this.props;
    const key = `${keyPrefix}${name}`;
    const model = {
      name,
      filename,
      contents,
    };
    localStorage.setItem(key, JSON.stringify(model));
  }

  render() {
    const { name } = this.props.model;
    const { contents } = this.state;

    return (
      <div>
        <SaveAsFile
          show={this.state.exportFile}
          contents={this.state.contents}
          onHide={this.onExport}
          onClose={this.onExport}
          defaultName={this.props.model.filename}
        />
        <ModalPushModelDialog
          show={this.state.pushDialog}
          onHide={this.onPush}
          model={this.state.contents}
          onClose={this.onPush}
        />
        <Card>
          <Card.Header>
            {`Model: ${name}`}
          </Card.Header>
          <Card.Body>
            <Navbar bg="light" variant=".dark">
              <Nav className="mr-auto">
                <Nav.Item>
                  <Nav.Link onClick={this.onVisualize} href="#">
                    Visualize
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link onClick={this.onPush}>
                    Push To Experiment
                  </Nav.Link>
                </Nav.Item>
              </Nav>
              <Nav className="ml-auto">
                <Nav.Item>
                  <Nav.Link
                    onClick={this.onExport}
                  >
                    Download
                  </Nav.Link>
                </Nav.Item>
                <NavDropdown as={Nav.Item} id="colorScheme" title="Color Scheme">
                  {themes.map((t, i) => (
                    <NavDropdown.Item key={t} eventKey={i} onClick={() => this.onTheme(t)}>
                      {t}
                    </NavDropdown.Item>
                  ))}
                </NavDropdown>
              </Nav>
            </Navbar>
            <div
              ref={(eleref) => this.eleref = eleref} // eslint-disable-line no-return-assign
            >
              <AceEditor
                className="mtb_model_editor"
                mode="python"
                theme={this.state.theme}
                showPrintMargin={false}
                editorProps={{
                  $blockScrolling: Infinity,
                }}
                enableBasicAutocompletion
                value={contents}
                onChange={this.onEditorChange}
                width={`${this.state.width}px`}
                height={`${this.state.height}px`}
              />
            </div>
            {this.state.visualize === true
              ? (
                <ModelVisualization
                  name={name}
                  xir={this.state.contents}
                />
              )
              : null}
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default ModelEditor;
