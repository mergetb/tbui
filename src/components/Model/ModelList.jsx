import React, { Component } from 'react';
import { Card, Table } from 'react-bootstrap';
import { Redirect, NavLink } from 'react-router-dom';
import AddDeleteItemButton from 'components/CustomButton/CustomButton';
import ModelFileDialog from 'components/Dialog/ModelFileDialog';

class ModelList extends Component {
  static sortModels(models) {
    return models.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });
  }

  constructor(props) {
    super(props);

    this.state = {
      models: [],
      modalOpen: false,
      redirect: null,
    };

    this.keyPrefix = 'mde_'; // "model editor data"
    this.onModalClose = this.onModalClose.bind(this);
    this.loadModelData = this.loadModelData.bind(this);
  }

  componentDidMount() {
    this.initLocalStorage();
  }

  onModalClose() {
    this.setState({
      modalOpen: false,
    });
  }

  handleAddModel() {
    this.setState({
      modalOpen: true,
    });
  }

  handleDelete(model) {
    // kill the localstorage if it exists.
    localStorage.removeItem(`${this.keyPrefix}${model.name}`);

    // update the local models state.
    this.setState((prevState) => ({
      models: prevState.models.filter((mdl) => mdl.name !== model.name),
    }));
  }

  initLocalStorage() {
    const models = [];
    const lsKeys = Object.keys(localStorage);

    // Look for all local storage keys that start with our prefix.
    for (let i = 0; i < lsKeys.length; i += 1) {
      if (lsKeys[i].startsWith(this.keyPrefix)) {
        const r = localStorage.getItem(lsKeys[i]);
        const model = JSON.parse(r);
        models.push(model);
      }
    }

    if (models.length > 0) {
      this.setState({
        models,
      });
    }
  }

  loadModelData(name, filename, contents) {
    const m = {
      name,
      filename,
      contents,
    };

    this.setState(
      (prevState) => ({
        models: [...prevState.models, m],
        redirect: {
          pathname: `/models/edit/${name}`,
          state: {
            model: m,
            keyPrefix: this.keyPrefix,
          },
        },
      }),
    );
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <div>
        <ModelFileDialog
          show={this.state.modalOpen}
          onSubmit={this.loadModelData}
          onClose={this.onModalClose}
        />
        <Card>
          <Card.Header>Local Models</Card.Header>
          <Card.Body>
            <Table striped hover>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Filename</th>
                  <th>{ }</th>
                </tr>
              </thead>
              <tbody>
                {this.state.models.map((m) => (
                  <tr key={m.name}>
                    <td key={`${m.name}.1`}>
                      <NavLink
                        to={{
                          pathname: `models/edit/${m.name}`,
                          state: {
                            model: m,
                            keyPrefix: this.keyPrefix,
                          },
                        }}
                      >
                        {m.name}
                      </NavLink>
                    </td>
                    <td key={`${m.name}.2`}>
                      {m.filename}
                    </td>
                    <td className="tc-actions text-right">
                      <AddDeleteItemButton
                        add={false}
                        item="Model"
                        onClick={() => this.handleDelete(m)}
                      />
                    </td>
                  </tr>
                ))}
                <tr>
                  <td className="tc-actions text-left">
                    <AddDeleteItemButton
                      add
                      item="Model"
                      onClick={() => this.handleAddModel()}
                    />
                  </td>
                  <td />
                  <td />
                </tr>
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default ModelList;
