import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

import ModelEditor from 'components/Model/ModelEditor.jsx';

class ModelView extends Component {
  render() {
    const { model, keyPrefix } = this.props.location.state;

    return (
      <Container fluid>
        <Row>
          <Col md={12}>
            <ModelEditor
              model={model}
              keyPrefix={keyPrefix}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default ModelView;
