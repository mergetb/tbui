
import Configuration from 'views/Configuration/Configuration';
import UsersView from 'views/UsersView/UsersView';
import UserProfile from 'components/UserProfile/UserProfile';
import ProjectsView from 'views/ProjectsView/ProjectsView';
import ProjectView from 'views/ProjectView/ProjectView';
import Callback from 'components/Callback/Callback';
import DebugView from 'views/DebugView/DebugView';
import ExperimentView from 'views/ExperimentView/ExperimentView';
import ExperimentVersion from 'components/Experiment/ExperimentVersion';
import RealizationView from 'views/RealizationView/RealizationView';
import MaterializationView from 'components/Materialization/MaterializationView';
import ModelList from 'components/Model/ModelList';
import ModelView from 'components/Model/ModelView';
import NewThing from 'components/NewThing/NewThing';
import ShowAllThingsView from 'components/ShowAllThings/ShowAllThingsView';
import NotFound from 'components/APIError/NotFound';
import AccessDenied from 'components/APIError/AccessDenied';
import Unauthorized from 'components/APIError/Unauthorized';
import Landing from 'components/Landing/Landing';

const dashboardRoutes = [
  {
    path: '/',
    component: ShowAllThingsView,
    authentication: true,
    header: true,
    name: 'Your Testbed',
    icon: 'pe-7s-global',
  },
  {
    path: '/landing',
    component: Landing,
    authentication: false,
    header: false,
    name: 'Landing',
  },
  {
    path: '/projects',
    name: 'Projects',
    icon: 'fa fa-book',
    component: ProjectsView,
    authentication: true,
    header: true,
  },
  {
    path: '/models',
    name: 'Models',
    icon: 'fa fa-wrench',
    component: ModelList,
    authentication: true,
    header: true,
  },
  {
    path: '/models/edit/:name',
    name: 'Model View',
    authentication: true,
    component: ModelView,
    header: false,
  },
  {
    path: '/users',
    name: 'User Profiles',
    icon: 'pe-7s-user',
    component: UsersView,
    authentication: true,
    header: true,
  },
  {
    name: 'User',
    path: '/users/:username',
    authentication: true,
    component: UserProfile,
  },
  {
    path: '/configuration',
    name: 'Configuration',
    icon: 'pe-7s-settings',
    component: Configuration,
    header: true,
  },
  {
    name: 'Project',
    path: '/projects/:projectname',
    authentication: true,
    component: ProjectView,
  },
  {
    path: '/projects/:projectname/experiments/:experimentname',
    name: 'Experiments',
    icon: 'pe-7s-study',
    component: ExperimentView,
    authentication: true,
  },
  {
    path: '/projects/:projectname/experiments/:experimentname/realizations/:realizationname',
    component: RealizationView,
    authentication: true,
  },
  {
    path: '/projects/:projectname/experiments/:experimentname/realizations/:realizationname/materializations/:materializationname',
    component: MaterializationView,
    authentication: true,
  },
  {
    path: '/callback',
    component: Callback,
  },
  {
    path: '/debug',
    component: DebugView,
    name: 'Debug Data',
    header: false,
  },
  {
    path: '/projects/:projectname/experiments/:experimentname/src/:xhash',
    component: ExperimentVersion,
    authentication: true,
  },
  {
    path: '/create/:thing/:arg1?/:arg2?/:arg3?', // GTL kinds of a cheat to use the path this way
    // the NewThings are created via a Link though.
    // is there a wy to pass args via a Link?
    component: NewThing,
  },
  {
    path: '/unauthorized',
    component: Unauthorized,
  },
  {
    path: '/accessdenied',
    component: AccessDenied,
  },
  {
    path: '/notfound',
    component: NotFound,
  },
];

export default dashboardRoutes;
