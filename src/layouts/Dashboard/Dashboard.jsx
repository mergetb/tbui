import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import { connect } from 'react-redux';
import { Container, Row, Col } from 'react-bootstrap';

import Header from 'components/Header/Header';
import Footer from 'components/Footer/Footer';
import Notifications from 'components/Notifications/Notifications';
import dashboardRoutes from 'routes/dashboard';

import Landing from 'components/Landing/Landing';

import { getIsAuthenticated, getNotifications } from 'reducers/selectors';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.toggleNotification = this.toggleNotification.bind(this);
    this.state = {
      showNotifications: false,
    };
  }

  toggleNotification() {
    const { showNotifications } = this.state;
    this.setState({
      showNotifications: !showNotifications,
    });
  }

  render() {
    const { isAuthenticated } = this.props;
    const { showNotifications } = this.state;

    return (
      <>
        <Header {...this.props} toggleNotification={this.toggleNotification} />
        <Container fluid className="maincontainer">
          <Row>
            <Col>
              {showNotifications && <Notifications />}
              <Switch>
                {dashboardRoutes.map((prop) => {
                  if (prop.redirect) {
                    return <Redirect from={prop.path} to={prop.to} key={prop} />;
                  }
                  if (!prop.authentication || (prop.authentication && isAuthenticated)) {
                    return (
                      <Route exact path={prop.path} component={prop.component} key={prop} />
                    );
                  }
                  return null;
                })}
                <Route path="/" component={Landing} />
              </Switch>
            </Col>
          </Row>
        </Container>
        <Footer />
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: getIsAuthenticated(state),
  notifications: getNotifications(state),
});

export default connect(mapStateToProps)(Dashboard);
