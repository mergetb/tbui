import {
  SET_NOTIFICATION_ACTION,
  CLEAR_NOTIFICATION_ACTION,
  CLEAR_ALL_NOTIFICATIONS_ACTION,
} from 'actions/notification_actions';

const notificationReduction = (state = [], action) => {
  const { type } = action;
  switch (type) {
    case SET_NOTIFICATION_ACTION:
      return [...state, action.payload];
    case CLEAR_NOTIFICATION_ACTION:
      return state.filter((item) => item.id !== action.item.id);
    case CLEAR_ALL_NOTIFICATIONS_ACTION:
      return [];
    default:
      return state;
  }
};

export default {
  notifications: notificationReduction,
};
