//
// Selectors for redux state abstracted here. This allows us to change
// shape of state if needed and more precisely targets bits of state
// needed by components.
//
export const getUsers = (state) => {
  if (!state || !state.users) {
    return null;
  }
  return state.users;
};

export const getUser = (state, username) => {
  const users = getUsers(state);
  if (!users) {
    return null;
  }
  return users[username];
};

export const getUserActive = (state) => state.userNotActive === false;

export const getProjects = (state) => {
  if (!state.projects) {
    return null;
  }
  return state.projects;
};

export const getProject = (state, projectname) => {
  const projects = getProjects(state);
  if (!projects) {
    return null;
  }
  return projects[projectname];
};

export const getAuth = (state) => state.auth;
export const getAuthdProfile = (state) => state.auth.profile;
export const getIsAuthenticated = (state) => state.auth.isAuthenticated;
export const getIsTokenExpired = (state) => state.auth.tokenExpired;
export const getHealth = (state) => state.health;
export const getMtzTransition = (state) => state.mtzTransition;

export const getExperiments = (state, projectname) => {
  // See src/reducers/tbapi_reducers.js for data structure.
  const allExperiments = state.experiments;
  if (!allExperiments) {
    return null;
  }
  const exps = allExperiments[projectname];
  if (!exps) {
    return null;
  }
  return exps;
};

export const getExperiment = (state, projectname, experimentname) => {
  // exps is { expname: { data }, expname: {data}, ... }
  const exps = getExperiments(state, projectname);
  if (!exps) {
    return null;
  }
  return exps[experimentname];
};

export const getExpXdcs = (state, pid, eid) => {
  if (!state.expxdcs) return null;
  if (!state.expxdcs[pid]) return null;
  return state.expxdcs[pid][eid];
};

export const getRealizations = (state, pid, eid) => {
  if (!state.realizations) return null;
  if (!state.realizations[pid]) return null;
  return state.realizations[pid][eid];
};

export const getRealization = (state, pid, eid, rid) => {
  if (!state.realization) return null;
  const pideid = [pid, eid].join(); // horrible!
  if (!state.realization[pideid]) return null;
  return state.realization[pideid][rid];
};

export const getMaterializationStatus = (state, pid, eid, rid) => {
  if (!state.materializationStatus) return null;
  const id = [pid, eid].join(); // horrible!
  if (!state.materializationStatus[id] || !state.materializationStatus[id][rid]) return null;
  return state.materializationStatus[id][rid];
};

export const getMaterialization = (state, pid, eid, rid) => {
  if (!state.materialization) return null;
  const pideid = [pid, eid].join(); // even more horrible!
  if (!state.materialization[pideid] || !state.materialization[pideid][rid]) return null;
  return state.materialization[pideid][rid];
};

export const getMtzStarting = (state, pid, eid, rid) => {
  if (!state.mtzStarting) return null;
  const id = [pid, eid, rid].join();
  if (!state.mtzStarting[id]) return null;
  return state.mtzStarting[id];
};

export const getExperimentVersions = (state, pid, eid) => {
  if (!state.expversions) return null;
  if (!state.expversions[pid]) return null;
  return state.expversions[pid][eid];
};

export const getExperimentVersion = (state, pid, eid, hash) => {
  if (!state.expversion) return null;
  const pideid = [pid, eid].join(); // horrible!
  if (!state.expversion[pideid]) return null;
  return state.expversion[pideid][hash];
};

export const getXdcToken = (state, pid, eid, xdcname) => {
  if (!state.xdctoken) return null;
  const pideid = [pid, eid].join();
  if (!state.xdctoken[pideid]) return null;
  return state.xdctoken[pideid][xdcname];
};
export const getXdcTokens = (state, pid, eid) => {
  if (!state.xdctoken) return null;
  const pideid = [pid, eid].join();
  if (!state.xdctoken[pideid]) return null;
  // return the dict of xcdid -> tokens known about.
  return state.xdctoken[pideid];
};

export const getMembers = (state, projectname) => {
  const allMembers = state.members;
  if (!allMembers) {
    return null;
  }
  const projMembers = allMembers[projectname];
  if (!projMembers) {
    return null; // note that {} != null.
  }
  return projMembers;
};

export const getMember = (state, projectname, membername) => {
  // members is { membername: { data }, membername: { data }, ... }
  const members = getMembers(state, projectname);
  if (!members) {
    return null;
  }
  return members[membername];
};

export const getPublicKeys = (state, uid) => {
  if (!state.pubkeys || !uid) return null;
  return state.pubkeys[uid].keys;
};

export const getModelNet = (state, id) => state.modelNetsById[id];
export const getModelNets = (state) => state.modelNetsById;
export const getModelNode = (s, id) => (s.modelNodesById[id] ? s.modelNodesById[id] : null);
export const getModelNodes = (state) => state.modelNodesById;
export const getModelLink = (state, id) => state.modelLinksById[id];
export const getModelLinks = (state) => state.modelLinksById;
export const getModelEndpoint = (state, id) => state.modelEndpointsById[id];
export const getModelEndpoints = (state) => state.modelEndpointsById;

// these hardcoded strings should be pulled in from the reducers/actions...
// fetching is key'd by path/url
const isFetching = (state, key) => {
  if (state.fetching && state.fetching[key]) {
    return state.fetching[key];
  }
  return null;
};
export const isFetchingUser = (state, username) => isFetching(state, `users/${username}`);
export const isFetchingUsers = (state) => isFetching(state, 'users');

export const isFetchingProject = (state, id) => isFetching(state, `projects/${id}`);
export const isFetchingProjects = (state) => isFetching(state, 'projects');

export const isFetchingHealth = (state) => isFetching(state, 'health');

export const isFetchingExperiments = (state, projectname) => isFetching(state, `projects/${projectname}/experiments`);
export const isFetchingExperiment = (state, projectname, expname) => isFetching(state, `projects/${projectname}/experiments/${expname}`);

export const isFetchingExperimentVersions = (state, projectname, expname) => isFetching(state, `projects/${projectname}/experiments/${expname}/src`);
export const isFetchingExperimentVersion = (state, projectname, expname, hash) => isFetching(state, `projects/${projectname}/experiments/${expname}/src/${hash}`);

export const isFetchingMembers = (state, projectname) => isFetching(state, `projects/${projectname}/members`);
export const isFetchingMember = (state, projectname, membername) => isFetching(state, `projects/${projectname}/members/${membername}`);

export const isFetchingRealization = (state, pid, eid, rid) => isFetching(state, `projects/${pid}/experiments/${eid}/realizations/${rid}`);
export const isFetchingRealizations = (state, pid, eid) => isFetching(state, `projects/${pid}/experiments/${eid}/realizations`);

export const isFetchingExpXdcs = (state, pid, eid) => isFetching(state, `projects/${pid}/experiments/${eid}/xdc`);

export const isFetchingXdcToken = (state, pid, eid, xdcid) => isFetching(state, `projects/${pid}/experiments/${eid}/xdc/${xdcid}/token`);

export const isFetchingMaterializationStatus = (state, pid, eid, rid) => isFetching(state, `projects/${pid}/experiments/${eid}/realizations${rid}/materialization/status`);

export const isFetchingMaterialization = (state, pid, eid, rid) => isFetching(state, `projects/${pid}/experiments/${eid}/realizations${rid}/materialization`);

export const getNotifications = (state) => state.notifications;

export const isFetchingPublicKeys = (state, uid) => isFetching(state, `users/${uid}/keys`);

export default {
  getUsers,
  getUser,
  getUserActive,
  getProjects,
  getProject,
  getExperiments,
  getExperiment,
  getExperimentVersion,
  getExperimentVersions,
  getMembers,
  getMember,
  getRealization,
  getRealizations,
  getExpXdcs,
  getXdcToken,
  getXdcTokens,
  getHealth,
  getAuth,
  getAuthdProfile,
  getIsAuthenticated,
  getMaterialization,
  getMaterializationStatus,
  getMtzStarting,
  getPublicKeys,

  isFetchingUser,
  isFetchingUsers,
  isFetchingHealth,
  isFetchingProject,
  isFetchingProjects,
  isFetchingExperiment,
  isFetchingExperimentVersion,
  isFetchingExperimentVersions,
  isFetchingExperiments,
  isFetchingMember,
  isFetchingMembers,
  isFetchingExpXdcs,
  isFetchingXdcToken,
  isFetchingRealization,
  isFetchingRealizations,
  isFetchingMaterialization,
  isFetchingMaterializationStatus,
  isFetchingPublicKeys,

  getNotifications,
};
