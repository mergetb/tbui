#!/usr/bin/env bash

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# build and deploy the tbui container
#
#

set -e

./certgen.sh
registry=${REGISTRY:-quay.io}
TAG=${TAG:-latest}
ORG=${ORG:-mergetb}

docker build $BUILD_ARGS -f tbui.dock -t $registry/$ORG/tbui:$TAG .

if [[ ! -z "$PUSH" ]]; then
  docker push $registry/$ORG/tbui:$TAG
fi

