Web front end for Merge-based testbeds. 

Very quick start:

```shell
npm install 
./run.sh
```

Add the following to `/etc/hosts`

```
127.0.0.1  portal.mergetb.net
```

