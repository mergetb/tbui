#!/bin/bash

HOST=${HOST:-launch.mergetb.net}
OU=${OU:-mergetb}
DHKEYSIZE=${DHKEYSIZE:-1024}


if [[ ! -f /tmp/cfssl ]]; then
  curl -o /tmp/cfssl -L https://pkg.cfssl.org/R1.2/cfssl_linux-amd64
  chmod +x /tmp/cfssl
fi

if [[ ! -f /tmp/cfssljson ]]; then
  curl -o /tmp/cfssljson -L https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64
  chmod +x /tmp/cfssljson
fi

if [[ ! -f /tmp/csr.json ]]; then
cat > /tmp/csr.json <<EOF
{
    "hosts": [ "$HOST" ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C":  "US",
            "L":  "Marina del Rey",
            "O":  "The Merge Testbed Framework",
            "OU": "$OU",
            "ST": "California"
        }
    ]
}
EOF
fi

if [[ ! -f /tmp/cfssl.json ]]; then
cat > /tmp/cfssl.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "87600h",
      "usages": [
        "signing",
        "key encipherment",
        "server auth",
        "client auth"
      ]
    }
  }
}
EOF
fi


if [[ ! -f ca-key.pem ]] || [[ ! -f ca.pem ]]; then
  /tmp/cfssl genkey -initca /tmp/csr.json | /tmp/cfssljson -bare ca
fi

if [[ ! -f server-key.pem ]] || [[ ! -f server.pem ]]; then
echo '{"key":{"algo":"rsa","size":2048}}' \
  | /tmp/cfssl gencert \
    -ca=ca.pem \
    -ca-key=ca-key.pem \
    -config=/tmp/cfssl.json \
    -hostname="$HOST" - | /tmp/cfssljson -bare server
fi

cat ca.pem server.pem > domain.pem

if [[ ! -f dhparam.pem ]]; then
    openssl dhparam -out dhparam.pem $DHKEYSIZE
fi

